function [psi_down_t_b_up_o, b_star_down_t] = get_psi(sne_nu_up_t_b, param, sne_param)
    
    % Probability of each possible outcome given the arrival time and bid
    % distribution
    
    psi_down_t_b_up_o = zeros(param.T, sne_param.n_k, sne_param.n_o);
    b_star_down_t = zeros(param.T, 1);

    nu_up_t_geq_b = cumsum(sne_nu_up_t_b, 2, 'reverse');
    nu_up_t_g_b = nu_up_t_geq_b - sne_nu_up_t_b;
    
    for t = 1 : param.T
        % Index of bids that will surely access fast lane
        i_b_1 = find(nu_up_t_geq_b(t,:) + param.epsilon <= param.s_1);
        psi_down_t_b_up_o(t,i_b_1,1) = 1;
        
        % Index of bids that will surely not access fast lane
        i_b_2 = find(nu_up_t_g_b(t,:) >= param.s_1);
        psi_down_t_b_up_o(t,i_b_2,1) = 0;
        
        % Index of inbetweeners
        i_b_12 = 1 : sne_param.n_k;
        i_b_12(i_b_1) = [];
        i_b_12(i_b_2) = [];
        psi_down_t_b_up_o(t,i_b_12,1) = (param.s_1 - nu_up_t_g_b(t,i_b_12)) ./ (sne_nu_up_t_b(t,i_b_12) + param.epsilon);
        
        % Threshold bid
        if ~isempty(i_b_12)
            i_b_star = i_b_12(end);
        else
            i_b_star = 1;
        end
        b_star_down_t(t) = sne_param.K(i_b_star);
    end
    
    psi_down_t_b_up_o(:,:,2) = 1 - psi_down_t_b_up_o(:,:,1);
end