% Write performance comparison results to csv file
function write_performance_comparison_csv(t_q_bar_min, c_bar, u_bar, param, fileprefix)
    % Header
    header = ["scheme", "t-q-bar", "t-q-bar-min", "t-q-bar-processed", "t-q-bar-min-processed", "c-bar", "c-bar-norm", "u-bar"];
    filename = [fileprefix, '-performance-comparison.csv'];
    fout = fopen(filename, 'w');
    for i = 1 : length(header) - 1
        fprintf(fout, '%s,', header(i));
    end
    fprintf(fout, '%s\n', header(end));
    fclose(fout);
    
    % Data
    schemes = (1 : 3).';  % 1 => karma, 2 => toll, 3 => no split
    data = [schemes, t_q_bar_min(1:3) / param.dt, t_q_bar_min(1:3), [t_q_bar_min(4); t_q_bar_min(2:3)] / param.dt, [t_q_bar_min(4); t_q_bar_min(2:3)], ...
        c_bar, c_bar / u_bar, u_bar * ones(3, 1)];
    dlmwrite(filename, data, '-append');
end