% Plot traffic function
function plot_traffic(fg, position, n_down_tstar_mu_alpha_u_t_o, n_down_t_o, t_q_down_t_o, t_analytical, U, r_analytical_down_u_t_o, r_analytical_down_t_o, t_q_analytical_down_t_o, param, sne_param)
    t_q_min_down_t_o = t_q_down_t_o * param.dt;

    n_max = max(n_down_t_o(:));
    n_analytical_max = max(r_analytical_down_t_o(:)) * param.dt;
    n_max = max([n_max, n_analytical_max]);
    t_q_max = max(t_q_min_down_t_o(:));
    t_q_analytical_max = max(t_q_analytical_down_t_o(:));
    t_q_max = max([t_q_max, t_q_analytical_max]);
    
    if ishandle(fg)
        close(fg);
    end
    
    figure(fg);
    fig = gcf;
    fig.Position = position;
    n_rows = sne_param.n_o;
    n_cols = 2;
    i_subplot = 1;

    for i_o = 1 : sne_param.n_o
        % Departures & queue plot
        subplot(n_rows, n_cols, i_subplot);
        hold on;
        
        stem(param.tstar_min, n_max * ones(param.n_tstar, 1), 'b--', 'Marker', 'none', 'LineWidth', 2);
        lgd_array = "t*";
        
        if i_o == 1
            data_to_plot = param.s_1 * ones(param.T, 1);
        else
            data_to_plot = param.s_2 * ones(param.T, 1);
        end
        plot(param.t_min, data_to_plot, 'r--', 'LineWidth', 2);
        lgd_array = [lgd_array; "Capacity"];

        n_down_t_so_far = zeros(param.T, 1);
        for i_tstar = 1 : param.n_tstar
            for i_mu = 1 : param.n_mu
                for i_alpha = 1 : param.n_alpha
                    for i_u = 1 : param.n_u
                        stem(param.t_min, n_down_t_o(:,i_o) - n_down_t_so_far, 'LineWidth', 2);
                        n_down_t_so_far = n_down_t_so_far + squeeze(n_down_tstar_mu_alpha_u_t_o(i_tstar,i_mu,i_alpha,i_u,:,i_o));

                        lgd_text = ['Karma departures '];
                        if param.n_tstar > 1
                            lgd_text = [lgd_text, 't*=', num2str(param.tstar(i_tstar)), ' '];
                        end
                        if param.n_mu > 1
                            lgd_text = [lgd_text, '\mu=', num2str(i_mu), ' '];
                        end
                        if param.n_alpha > 1
                            alpha = param.Alpha(i_alpha);
                            if alpha > 0.99 && alpha < 1
                                alpha_str = num2str(alpha, '%.3f');
                            else
                                alpha_str = num2str(alpha, '%.2f');
                            end
                            lgd_text = [lgd_text, '\alpha=', alpha_str, ' '];
                        end
                        lgd_text = [lgd_text, 'u=', num2str(param.U_down_mu(i_mu,i_u))];
                        lgd_array = [lgd_array; lgd_text];
                    end
                end
            end
        end

        r_analytical_down_t_so_far = zeros(length(t_analytical), 1);
        for i_u = 1 : length(U)
            plot(t_analytical, (r_analytical_down_t_o(:,i_o) - r_analytical_down_t_so_far) * param.dt, 'LineWidth', 2);
            r_analytical_down_t_so_far = r_analytical_down_t_so_far + squeeze(r_analytical_down_u_t_o(i_u,:,i_o)).';

            lgd_array = [lgd_array; ['Toll departure rate u=', num2str(U(i_u)), ' [veh/', num2str(param.dt), 'min]']];
        end

        axis tight;
        axes = gca;

        axes.YLim = [0, n_max];

        title_str = ['Departures & queue in lane ', num2str(sne_param.O(i_o))];
        axes.Title.String = title_str;

        axes.XLabel.String = 'Departure time [min]';
        axes.YLabel.String = 'Mass of vehicles';

        lgd = legend(lgd_array);
        lgd.Location = 'bestoutside';

        axes.Title.FontName = 'ubuntu';
        axes.XLabel.FontName = 'ubuntu';
        axes.YLabel.FontName = 'ubuntu';
        axes.XAxis.FontName = 'ubuntu';
        axes.YAxis.FontName = 'ubuntu';
        lgd.FontName = 'ubuntu';

        axes.Title.FontSize = sne_param.plot_font_size;
        axes.XLabel.FontSize = sne_param.plot_font_size;
        axes.YLabel.FontSize = sne_param.plot_font_size;
        axes.XAxis.FontSize = sne_param.plot_font_size;
        axes.YAxis.FontSize = sne_param.plot_font_size;
        lgd.FontSize = sne_param.plot_font_size;

        i_subplot = i_subplot + 1;

        % Waiting time plot
        subplot(n_rows, n_cols, i_subplot);
        hold on;
        
        stem(param.tstar_min, t_q_max * ones(param.n_tstar, 1), 'b--', 'Marker', 'none', 'LineWidth', 2);
        
        stem(param.t_min, t_q_min_down_t_o(:,i_o), 'LineWidth', 2);
        
        plot(t_analytical, t_q_analytical_down_t_o(:,i_o), 'g', 'LineWidth', 2);
        
        axis tight;
        axes = gca;

        axes.YLim = [0, t_q_max];

        title_str = ['Waiting time in lane ', num2str(sne_param.O(i_o))];
        axes.Title.String = title_str;

        axes.XLabel.String = 'Departure time [min]';
        axes.YLabel.String = 'Waiting time [min]';

        lgd = legend('t*', 'Karma', 'Toll');
        lgd.Location = 'bestoutside';

        axes.Title.FontName = 'ubuntu';
        axes.XLabel.FontName = 'ubuntu';
        axes.YLabel.FontName = 'ubuntu';
        axes.XAxis.FontName = 'ubuntu';
        axes.YAxis.FontName = 'ubuntu';
        lgd.FontName = 'ubuntu';

        axes.Title.FontSize = sne_param.plot_font_size;
        axes.XLabel.FontSize = sne_param.plot_font_size;
        axes.YLabel.FontSize = sne_param.plot_font_size;
        axes.XAxis.FontSize = sne_param.plot_font_size;
        axes.YAxis.FontSize = sne_param.plot_font_size;
        lgd.FontSize = sne_param.plot_font_size;

        i_subplot = i_subplot + 1;
    end
    sgtitle('Traffic conditions', 'FontSize', 20, 'FontName', 'ubunutu', 'FontWeight', 'bold');
end