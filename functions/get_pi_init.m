function pi_down_tstar_mu_alpha_u_k_up_t_b_init = get_pi_init(param, sne_param)
    % Gets initial policy guess

    % Initial time of departure - possibly dependant on desired arrival time type
    prob_down_tstar_up_t_init = zeros(param.n_tstar, param.T);
    switch sne_param.t_policy_initialization
        case 0
            % Depart at ideal arrival time
            for i_tstar = 1 : param.n_tstar
                t = param.tstar(i_tstar);
                prob_down_tstar_up_t_init(i_tstar,t) = 1;
            end
            
        case 1
            % Uniform random
            for i_tstar = 1 : param.n_tstar
                prob_down_tstar_up_t_init(i_tstar,:) = 1 / param.T;
            end
            
        case 2
            % According to analytical equilibrium
            for i_tstar = 1 : param.n_tstar
                t_peak = round(param.tpeak(i_tstar));
                prob_down_tstar_up_t_init(i_tstar,1:t_peak) = 1 / (param.alpha - param.beta);
                prob_down_tstar_up_t_init(i_tstar,t_peak+1:end) = 1 / (param.alpha + param.gamma);
                prob_down_tstar_up_t_init(i_tstar,:) = prob_down_tstar_up_t_init(i_tstar,:) / sum(prob_down_tstar_up_t_init(i_tstar,:));
            end
            
    end
    
    % Initial bidding policy - independent of agent types
    prob_down_mu_u_k_up_b_init = zeros(param.n_mu, param.n_u, sne_param.n_k, sne_param.n_k);
    for i_mu = 1 : param.n_mu
        for i_u = 1 : param.n_u
            for i_k = 1 : sne_param.n_k
                switch sne_param.b_policy_initialization
                    case 0
                        % Bid urgency
                        if param.n_u > 1
                            i_b = min([i_u, i_k]);
                        else
                            % Bid 1 if there is only 1 urgency level
                            i_b = min([2, i_k]);
                        end
                        prob_down_mu_u_k_up_b_init(i_mu,i_u,i_k,i_b) = 1;
                        
                    case 1
                        % Bid 0.5 * u / u_max * k (~ 'bid half if urgent')
                        b = round(0.5 * param.U_down_mu(i_mu,i_u) / param.umax_down_mu(i_mu) * sne_param.K(i_k));
                        i_b = sne_param.K == b;
                        prob_down_mu_u_k_up_b_init(i_mu,i_u,i_k,i_b) = 1;
                        
                    case 2
                        % Bid 1 * u / u_max * k (~ 'bid all if urgent')
                        b = round(param.U_down_mu(i_u,i_u) / param.umax_down_mu(i_mu) * sne_param.K(i_k));
                        i_b = sne_param.K == b;
                        prob_down_mu_u_k_up_b_init(i_mu,i_u,i_k,i_b) = 1;
                        
                    case 3
                        % Bid uniform random
                        prob_down_mu_u_k_up_b_init(i_mu,i_u,i_k,1:i_k) = 1 / i_k;
    
                end
            end
        end
    end
    
    % Duplicate for agent types
    if sne_param.karma_initialization == 10     % Get from file
        load(sne_param.initialization_file, 'sne_pi_down_tstar_mu_alpha_u_k_up_t_b');
        pi_down_tstar_mu_alpha_u_k_up_t_b_init = sne_pi_down_tstar_mu_alpha_u_k_up_t_b;
    else
        pi_down_tstar_mu_alpha_u_k_up_t_b_init = einsum('in,jlmo,kp->ijklmno', prob_down_tstar_up_t_init, prob_down_mu_u_k_up_b_init, ones(param.n_alpha, 1));
    end
end

