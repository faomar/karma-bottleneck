function [n_down_tstar_mu_alpha_u_t_o, n_down_t_o, q_down_t_o, t_q_down_t_o] = get_queue(sne_pi_down_tstar_mu_alpha_u_k_up_t_b, sne_d_up_tstar_mu_alpha_u_k, sne_nu_up_t_b, psi_down_t_b_up_o, param, sne_param)
% n_down_tstar_mu_alpha_u_t_o = mass of departures per timestep in each
% lane by agent type and urgency
% n_down_t_o = mass of departures per timestep in each lane
% q_down_t_o = mass of queue per timestep in each lane
% t_q_down_t_o = queuing time per timestep in each lane

% Mass of departures per timestep in each lane by agent type and urgency
n_down_tstar_mu_alpha_u_t_o = einsum('ijklm, ijklmno, nop->ijklnp', sne_d_up_tstar_mu_alpha_u_k, sne_pi_down_tstar_mu_alpha_u_k_up_t_b, psi_down_t_b_up_o);

% Mass of departures per timestep in each lane
n_down_t_o = einsum('ij,ijl->il', sne_nu_up_t_b, psi_down_t_b_up_o);

% Mass of queue per timestep in each lane
q_down_t_o = zeros(param.T, sne_param.n_o);
q_down_t_o(1,1) = max(0, n_down_t_o(1,1) - param.s_1);
q_down_t_o(1,2) = max(0, n_down_t_o(1,2) - param.s_2);

for t = 2 : param.T
    q_down_t_o(t,1) = max(0, q_down_t_o(t-1,1) + n_down_t_o(t,1) - param.s_1);
    q_down_t_o(t,2) = max(0, q_down_t_o(t-1,2) + n_down_t_o(t,2) - param.s_2);
end

t_q_down_t_o = zeros(param.T, sne_param.n_o);
t_q_down_t_o(:,1) = q_down_t_o(:,1) / param.s_1;
t_q_down_t_o(:,2) = q_down_t_o(:,2) / param.s_2;

t_q_down_t_o(isnan(t_q_down_t_o)) = 0;

end