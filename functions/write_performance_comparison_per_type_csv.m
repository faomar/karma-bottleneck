% Write performance comparison per urgency type results to csv file
function write_performance_comparison_per_type_csv(t_q_bar_min_down_mu, c_bar_down_mu, c_bar_norm_down_mu, param, fileprefix)
    % Header
    header = ["scheme", "mu", "index", "t-q-bar", "t-q-bar-min", "t-q-bar-processed", "t-q-bar-min-processed", "c-bar", "c-bar-norm", "u-bar"];
    filename = [fileprefix, '-performance-comparison-per-type.csv'];
    fout = fopen(filename, 'w');
    for i = 1 : length(header) - 1
        fprintf(fout, '%s,', header(i));
    end
    fprintf(fout, '%s\n', header(end));
    fclose(fout);
    
    % Data
    schemes = (1 : 3)';  % 1 => karma, 2 => toll, 3 => no split
    for i_mu = 1 : param.n_mu
        if i_mu <= param.n_mu / 2
            index = schemes - 0.2 * (floor(param.n_mu / 2) + 1 - i_mu);
        else
            index = schemes + 0.2 * (i_mu - ceil(param.n_mu / 2));
        end
        mu_vec = i_mu * ones(3, 1);
        u_bar_vec = param.u_bar_down_mu(i_mu) * ones(3, 1);
        data = [schemes, mu_vec, index, t_q_bar_min_down_mu(1:3,i_mu) / param.dt, t_q_bar_min_down_mu(1:3,i_mu), ...
            [t_q_bar_min_down_mu(4,i_mu); t_q_bar_min_down_mu(2:3,i_mu)] / param.dt, [t_q_bar_min_down_mu(4,i_mu); t_q_bar_min_down_mu(2:3,i_mu)], ...
            c_bar_down_mu(:,i_mu), c_bar_norm_down_mu(:,i_mu), u_bar_vec];
        dlmwrite(filename, data, '-append');
    end
end