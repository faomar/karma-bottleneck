% Plot SNE karma distribution
function plot_sne_sigma(fg, position, sne_sigma_down_tstar_mu_alpha_up_k, param, sne_param)
persistent sne_sigma_plot
    if ~ishandle(fg)
        figure(fg);
        fig = gcf;
        fig.Position = position;
        sne_sigma_plot = cell(param.T, param.n_mu, param.n_alpha);
        sigma_max = max(sne_sigma_down_tstar_mu_alpha_up_k(:));
        n_rows = param.n_tstar * param.n_mu;
        n_cols = param.n_alpha;
        i_subplot = 1;            
        for i_tstar = 1 : param.n_tstar
            for i_mu = 1 : param.n_mu
                for i_alpha = 1 : param.n_alpha
                    subplot(n_rows, n_cols, i_subplot);
                    sne_sigma_plot{i_tstar,i_mu,i_alpha} = bar(sne_param.K, squeeze(sne_sigma_down_tstar_mu_alpha_up_k(i_tstar,i_mu,i_alpha,:)));
                    axis tight;
                    axes = gca;
                    
                    axes.YLim = [0, sigma_max];
                    
                    title_str = ['t*=', num2str(param.tstar(i_tstar))];
                    if param.n_mu > 1
                        title_str = [title_str, ' \mu=', num2str(i_mu)'];
                    end
                    if param.n_alpha > 1
                        alpha = param.Alpha(i_alpha);
                        if alpha > 0.99 && alpha < 1
                            alpha_str = num2str(alpha, '%.3f');
                        else
                            alpha_str = num2str(alpha, '%.2f');
                        end
                        title_str = [title_str, ' \alpha=', alpha_str];
                    end
                    axes.Title.String = title_str;
                    
                    axes.XLabel.String = 'Karma';
                    axes.YLabel.String = 'Distribution';
                    
                    axes.Title.FontName = 'ubuntu';
                    axes.XLabel.FontName = 'ubuntu';
                    axes.YLabel.FontName = 'ubuntu';
                    axes.XAxis.FontName = 'ubuntu';
                    axes.YAxis.FontName = 'ubuntu';

                    axes.Title.FontSize = sne_param.plot_font_size;
                    axes.XLabel.FontSize = sne_param.plot_font_size;
                    axes.YLabel.FontSize = sne_param.plot_font_size;
                    axes.XAxis.FontSize = sne_param.plot_font_size;
                    axes.YAxis.FontSize = sne_param.plot_font_size;

                    i_subplot = i_subplot + 1;
                end
            end
        end
        sgtitle('Karma distribution', 'FontSize', 20, 'FontName', 'ubunutu', 'FontWeight', 'bold');
    else
        sigma_max = max(sne_sigma_down_tstar_mu_alpha_up_k(:));
        for i_tstar = 1 : param.n_tstar
            for i_mu = 1 : param.n_mu
                for i_alpha = 1 : param.n_alpha
                    sne_sigma_plot{i_tstar,i_mu,i_alpha}.YData = squeeze(sne_sigma_down_tstar_mu_alpha_up_k(i_tstar,i_mu,i_alpha,:));
                    
                    sne_sigma_plot{i_tstar,i_mu,i_alpha,i_u}.Parent.YLim = [0, sigma_max];
                    
                    title_str = ['t*=', num2str(param.tstar(i_tstar))];
                    if param.n_mu > 1
                        title_str = [title_str, ' \mu=', num2str(i_mu)'];
                    end
                    if param.n_alpha > 1
                        alpha = param.Alpha(i_alpha);
                        if alpha > 0.99 && alpha < 1
                            alpha_str = num2str(alpha, '%.3f');
                        else
                            alpha_str = num2str(alpha, '%.2f');
                        end
                        title_str = [title_str, ' \alpha=', alpha_str];
                    end
                    sne_sigma_plot{i_tstar,i_mu,i_alpha}.Parent.Title.String = title_str;
                end
            end
        end
    end
end