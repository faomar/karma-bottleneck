function [t_analytical, r_analytical_down_t_o, q_analytical_down_t_o, t_q_analytical_down_t_o, U, prob_up_u, t_start_analytical_down_tstar_u, t_end_analytical_down_tstar_u, r_analytical_down_tstar_u_t_o, p_analytical_down_t_o, t_q_bar_analytical_down_tstar_u, c_bar_analytical_down_tstar_u] = get_queue_analytical(param, sne_param)
% This function closely follows Arnott et al., A structural model of
% peak-period congestion: A traffic bottleneck with elastic demand

% Everything in this function is in units of minutes
s_min = param.s / param.dt;
s_1_min = param.s_1 / param.dt;
s_2_min = param.s_2 / param.dt;
alpha_min = param.alpha / param.dt;
beta_min = param.beta / param.dt;
gamma_min = param.gamma / param.dt;

% Get time range of bottleneck
t_start = param.tstar_min(1) - gamma_min / (beta_min + gamma_min) / s_min;
t_end = param.tstar_min(1) + beta_min / (beta_min + gamma_min) / s_min;
dt_analytical = 0.5;
t_analytical = (t_start : dt_analytical : t_end).';
n_t_analytical = length(t_analytical);

% Get time of peak cogestion
if param.n_tstar == 1 || param.g_up_tstar(2) <=  param.tstar_min(2) / t_end
    t_peak = param.tstar_min(1) - beta_min * gamma_min / (alpha_min * (beta_min + gamma_min) * s_min);
    multi_peak = false;
else
    t_peak = param.tstar_min - beta_min * gamma_min / (alpha_min * (beta_min + gamma_min) * s_min) * param.g_up_tstar;
    multi_peak = true;
end

% Mass of new departures per timestep in each lane
r_analytical_down_t_o = zeros(n_t_analytical, sne_param.n_o);

% Fast lane is at capacity
r_analytical_down_t_o(:,1) = s_1_min;

% Slow lane obeys formula in Arott et al.
r_early = alpha_min / (alpha_min - beta_min) * s_2_min;
r_late = alpha_min / (alpha_min + gamma_min) * s_2_min;
for i_t_analytical = 1 : n_t_analytical
    t = t_analytical(i_t_analytical);
    switch multi_peak
        case false
            if t <= t_peak
                r_analytical_down_t_o(i_t_analytical,2) = r_early;
            else
                r_analytical_down_t_o(i_t_analytical,2) = r_late;
            end
        case true
    end
end

% Mass of queue per timestep in each lane
q_analytical_down_t_o = zeros(n_t_analytical, sne_param.n_o);

% Fast lane is at capacity
q_analytical_down_t_o(:,1) = 0;

% Slow lane obeys formula in Arott et al.
for i_t_analytical = 1 : n_t_analytical
    t = t_analytical(i_t_analytical);
    switch multi_peak
        case false
            if t <= t_peak
                q_analytical_down_t_o(i_t_analytical,2) = beta_min / (alpha_min - beta_min) * s_2_min * (t - t_start);
            else
                q_analytical_down_t_o(i_t_analytical,2) = beta_min / (alpha_min - beta_min) * s_2_min * (t_peak - t_start) ...
                    - gamma_min / (alpha_min + gamma_min) * s_2_min * (t - t_peak);
            end
        case true
    end
end

% Waiting time is straightforward from queue
t_q_analytical_down_t_o = zeros(n_t_analytical, sne_param.n_o);
t_q_analytical_down_t_o(:,1) = q_analytical_down_t_o(:,1) / s_1_min;
t_q_analytical_down_t_o(:,2) = q_analytical_down_t_o(:,2) / s_2_min;

t_q_analytical_down_t_o(isnan(t_q_analytical_down_t_o)) = 0;


%% Logic to find departure rates conditioned on urgency u
U = reshape(param.U_down_mu, [], 1);
U = unique(U);
U = sort(U);
n_u = length(U);

prob_up_u = zeros(n_u, 1);
for i_mu = 1 : param.n_mu
    for i_u = 1 : param.n_u
        i_u2 = find(U == param.U_down_mu(i_mu, i_u));
        prob_up_u(i_u2) = prob_up_u(i_u2) + param.g_up_mu(i_mu) * param.prob_down_mu_up_u(i_mu,i_u);
    end
end

r_analytical_down_tstar_u_t_o = zeros(n_u, n_t_analytical, sne_param.n_o);

s_1_left = s_1_min / s_min;
i_u = n_u;

t_start_analytical_down_tstar_u = zeros(param.n_tstar, n_u + 1);
t_end_analytical_down_tstar_u = zeros(param.n_tstar, n_u + 1);
t_start_analytical_down_tstar_u(:,end) = param.tstar_min;
t_end_analytical_down_tstar_u(:,end) = param.tstar_min;
while i_u > 1 && prob_up_u(i_u) < s_1_left
    for i_tstar = 1 : param.n_tstar
        t_start_analytical_down_tstar_u(i_tstar,i_u) = param.tstar_min(i_tstar) - gamma_min / (beta_min + gamma_min) * (t_end_analytical_down_tstar_u(i_tstar,i_u+1) - t_start_analytical_down_tstar_u(i_tstar,i_u+1) + param.g_up_tstar(i_tstar) * prob_up_u(i_u) / s_1_min);
        t_end_analytical_down_tstar_u(i_tstar,i_u) = param.tstar_min(i_tstar) + beta_min / (beta_min + gamma_min) * (t_end_analytical_down_tstar_u(i_tstar,i_u+1) - t_start_analytical_down_tstar_u(i_tstar,i_u+1) + param.g_up_tstar(i_tstar) * prob_up_u(i_u) / s_1_min);

        i_t1_analytical = find(t_analytical >= t_start_analytical_down_tstar_u(i_tstar,i_u) & t_analytical < t_start_analytical_down_tstar_u(i_tstar,i_u+1));
        i_t2_analytical = find(t_analytical >= t_end_analytical_down_tstar_u(i_tstar,i_u+1) & t_analytical < t_end_analytical_down_tstar_u(i_tstar,i_u));

        r_analytical_down_tstar_u_t_o(i_u,i_t1_analytical,1) = s_1_min;
        r_analytical_down_tstar_u_t_o(i_u,i_t2_analytical,1) = s_1_min;
    end
    
    s_1_left = s_1_left - prob_up_u(i_u);
    
    i_u = i_u - 1;
end

if s_1_left > 0
    t_start_analytical_down_tstar_u(i_u) = t_start;
    t_end_analytical_down_tstar_u(i_u) = t_end;
    
    i_t1_analytical = find(t_analytical >= t_start_analytical_down_tstar_u(i_u) & t_analytical < t_start_analytical_down_tstar_u(i_u+1));
    i_t2_analytical = find(t_analytical >= t_end_analytical_down_tstar_u(i_u+1) & t_analytical < t_end_analytical_down_tstar_u(i_u));
    
    r_analytical_down_tstar_u_t_o(i_u,i_t1_analytical,1) = s_1_min;
    r_analytical_down_tstar_u_t_o(i_u,i_t2_analytical,1) = s_1_min;
end
r_analytical_down_tstar_u_t_o(i_u,:,2) = (prob_up_u(i_u) - s_1_left) * s_min / s_2_min * r_analytical_down_t_o(:,2);

for i_u2 = 1 : i_u - 1
    t_start_analytical_down_tstar_u(i_u2) = t_start;
    t_end_analytical_down_tstar_u(i_u2) = t_end;
    
    r_analytical_down_tstar_u_t_o(i_u2,:,2) = prob_up_u(i_u2) * s_min / s_2_min * r_analytical_down_t_o(:,2);
end

%% Equilibrium optimal tolling price
p_analytical_down_t_o = zeros(n_t_analytical, sne_param.n_o);
for i_tstar = 1 : param.n_tstar
    p_start_analytical_down_u = zeros(n_u + 1, 1);
    p_end_analytical_down_u = zeros(n_u + 1, 1);
    for i_u2 = i_u : n_u
            p_start_analytical_down_u(i_u2+1) = p_start_analytical_down_u(i_u2) + U(i_u2) * beta_min * (t_start_analytical_down_tstar_u(i_tstar,i_u2+1) - t_start_analytical_down_tstar_u(i_tstar,i_u2));
            p_end_analytical_down_u(i_u2+1) = p_end_analytical_down_u(i_u2) + U(i_u2) * gamma_min * (t_end_analytical_down_tstar_u(i_tstar,i_u2) - t_end_analytical_down_tstar_u(i_tstar,i_u2+1));

            i_t1_analytical = find(t_analytical >= t_start_analytical_down_tstar_u(i_tstar,i_u2) & t_analytical < t_start_analytical_down_tstar_u(i_tstar,i_u2+1));
            i_t2_analytical = find(t_analytical >= t_end_analytical_down_tstar_u(i_tstar,i_u2+1) & t_analytical < t_end_analytical_down_tstar_u(i_tstar,i_u2));

            p_analytical_down_t_o(i_t1_analytical,1) = p_start_analytical_down_u(i_u2) + (t_analytical(i_t1_analytical) - t_start_analytical_down_tstar_u(i_tstar,i_u2)) * (p_start_analytical_down_u(i_u2+1) - p_start_analytical_down_u(i_u2)) / (t_start_analytical_down_tstar_u(i_tstar,i_u2+1) - t_start_analytical_down_tstar_u(i_tstar,i_u2));
            p_analytical_down_t_o(i_t2_analytical,1) = p_end_analytical_down_u(i_u2) + (t_end_analytical_down_tstar_u(i_tstar,i_u2) - t_analytical(i_t2_analytical)) * (p_end_analytical_down_u(i_u2+1) - p_end_analytical_down_u(i_u2)) / (t_end_analytical_down_tstar_u(i_tstar,i_u2) - t_end_analytical_down_tstar_u(i_tstar,i_u2+1));
    end
end

%% Equilibrium average waiting time per urgency group
t_q_bar_analytical_down_tstar_u = zeros(param.n_tstar, n_u);

% Nominal equilibrium cost (not weighted with urgency)
c_star_down_tstar = beta_min * gamma_min / ((beta_min + gamma_min) * s_min);

% Travellers going in slow lane incur average queuing delay c_star / (2 * alpha)
t_q_bar_analytical_down_tstar_u(:,1:i_u-1) = c_star_down_tstar / (2 * alpha_min);

% Travellers going in fast lane incur no queuing delay
t_q_bar_analytical_down_tstar_u(:,i_u+1:end) = 0;

% Edge travellers incur queuing delay sometimes and sometimes not
t_q_bar_analytical_down_tstar_u(:,i_u) = (prob_up_u(i_u) - s_1_left) / prob_up_u(i_u) * c_star_down_tstar / (2 * alpha_min);

%% Equilibrium cost per urgency group (not counting monetary price)
c_bar_analytical_down_tstar_u = zeros(param.n_tstar, n_u);

% Travellers going in slow lane incur c_star (weighed with their urgency)
for i_u2 = 1 : i_u - 1
    c_bar_analytical_down_tstar_u(:,i_u2) = U(i_u2) * c_star_down_tstar;
end

% Travellers going in fast lane incur cost of queue-free travel in their
% 'average' departure times
for i_u2 = i_u + 1 : n_u
    c_bar_analytical_down_tstar_u(:,i_u2) = U(i_u2) * beta_min * (param.tstar_min - (t_start_analytical_down_tstar_u(:,i_u2) + t_start_analytical_down_tstar_u(:,i_u2+1)) / 2) / 2;
    
    c_bar_analytical_down_tstar_u(:,i_u2) = c_bar_analytical_down_tstar_u(:,i_u2) ...
        + U(i_u2) * gamma_min * ((t_end_analytical_down_tstar_u(:,i_u2+1) + t_end_analytical_down_tstar_u(:,i_u2)) / 2 - param.tstar_min) / 2;
end

% Edge travellers incur cost of slow lane sometimes and fast lane sometimes
c_bar_analytical_down_tstar_u(:,i_u) = (prob_up_u(i_u) - s_1_left) / prob_up_u(i_u) * U(i_u) * c_star_down_tstar;

c_bar_analytical_down_tstar_u(:,i_u) = c_bar_analytical_down_tstar_u(:,i_u) ...
    + s_1_left / prob_up_u(i_u) * U(i_u) * beta_min * (param.tstar_min - (t_start_analytical_down_tstar_u(:,i_u) + t_start_analytical_down_tstar_u(:,i_u+1)) / 2) / 2;
    
c_bar_analytical_down_tstar_u(:,i_u) = c_bar_analytical_down_tstar_u(:,i_u) ...
    + s_1_left / prob_up_u(i_u) * U(i_u) * gamma_min * ((t_end_analytical_down_tstar_u(:,i_u+1) + t_end_analytical_down_tstar_u(:,i_u)) / 2 - param.tstar_min) / 2;
end