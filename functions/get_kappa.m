function [kappa_down_k_t_b_o_up_kn, redistribution] = get_kappa(kappa_down_k_b_o_up_kn_pre_redist, prob_up_k_t_b, sne_nu_up_t_b, psi_down_t_b_up_o, n_down_t_o, param, sne_param)
    persistent A_matrix b_vector    % used for non-uniform time redistribution

    % Averge next karma before redistribution per time, bid and lane
    prob_up_t_b_o_kn_pre_redist = einsum('ijkl,imj,mjk->mjkl', kappa_down_k_b_o_up_kn_pre_redist, prob_up_k_t_b, psi_down_t_b_up_o);
    
    % Averge next karma before redistribution per time and lane
    prob_up_t_kn_pre_redist = einsum('ijkl,jk->il', prob_up_t_b_o_kn_pre_redist, ones(sne_param.n_k, sne_param.n_o));

    % Average next karma before redistribution
    prob_up_kn_pre_redist = einsum('ij,ik->j', prob_up_t_kn_pre_redist, ones(param.T, 1));

    % Average payment
    p_bar = max([param.k_bar - dot(prob_up_kn_pre_redist, sne_param.K), 0]);

    % Redistribution
    switch param.transition_mechanism
        
        case {0, 1}
            % Uniform redistribution
            p_bar_low = floor(p_bar);
            p_bar_high = p_bar_low + 1;
            saturation_adjustment_term = 0;
            for i = 1 : p_bar_low
                saturation_adjustment_term = saturation_adjustment_term + prob_up_kn_pre_redist(sne_param.n_k - i) * i;
            end
            p_bar_adjusted = (p_bar - saturation_adjustment_term) / sum(prob_up_kn_pre_redist(1:sne_param.n_k-p_bar_high));
            p_bar_adjusted_low = floor(p_bar_adjusted);
            p_bar_adjusted_high = p_bar_adjusted_low + 1;
            f_low = p_bar_adjusted_high - p_bar_adjusted;
            f_high =  1 - f_low;
            prob_down_kn_pre_redist_up_kn = zeros(sne_param.n_k, sne_param.n_k);
            for i_k = 1 : sne_param.n_k
                i_kn_low = min([i_k + p_bar_adjusted_low, sne_param.n_k]);
                i_kn_high = min([i_k + p_bar_adjusted_high, sne_param.n_k]);
                prob_down_kn_pre_redist_up_kn(i_k,i_kn_low) = f_low;
                prob_down_kn_pre_redist_up_kn(i_k,i_kn_high) = prob_down_kn_pre_redist_up_kn(i_k,i_kn_high) + f_high;
            end
            kappa_down_k_t_b_o_up_kn = einsum('ijkl,lm,no->injkm', kappa_down_k_b_o_up_kn_pre_redist, prob_down_kn_pre_redist_up_kn, ones(param.T, 1));
            
            redistribution = p_bar_adjusted;
            
        case 2
            % Redistribution to losers + 0 bidders only
            n_redist = sum(sne_nu_up_t_b(:,1), 1) + einsum('ij,ijk->k', sne_nu_up_t_b(:,2:end), psi_down_t_b_up_o(:,2:end,2));
            assert(n_redist > 0, 'Invalid parameters - no congestion');  % Just in case
            
            prob_down_redist_up_kn_pre_redist = (einsum('ijkl,ik->l', prob_up_t_b_o_kn_pre_redist(:,1,:,:), ones(param.T, sne_param.n_o)) + einsum('ijkl,ij->l', prob_up_t_b_o_kn_pre_redist(:,2:end,2,:), ones(param.T, sne_param.n_k - 1))) / n_redist;
            
            r_redist = p_bar / n_redist;
            r_redist_low = floor(r_redist);
            r_redist_high = r_redist_low + 1;
            saturation_adjustment_term = 0;
            for i = 1 : r_redist_low
                saturation_adjustment_term = saturation_adjustment_term + prob_down_redist_up_kn_pre_redist(sne_param.n_k - i) * i;
            end
            r_redist_adjusted = (r_redist - saturation_adjustment_term) / sum(prob_down_redist_up_kn_pre_redist(1:sne_param.n_k-r_redist_high));
            r_redist_adjusted_low = floor(r_redist_adjusted);
            r_redist_adjusted_high = r_redist_adjusted_low + 1;
            f_low = r_redist_adjusted_high - r_redist_adjusted;
            f_high =  1 - f_low;
            prob_down_b_o_kn_pre_redist_up_kn = zeros(sne_param.n_k, sne_param.n_o, sne_param.n_k, sne_param.n_k);
            for i_k = 1 : sne_param.n_k
                prob_down_b_o_kn_pre_redist_up_kn(2:end,1,i_k,i_k) = 1;
                
                i_kn_low = min([i_k + r_redist_adjusted_low, sne_param.n_k]);
                i_kn_high = min([i_k + r_redist_adjusted_high, sne_param.n_k]);
                
                prob_down_b_o_kn_pre_redist_up_kn(1,:,i_k,i_kn_low) = f_low;
                prob_down_b_o_kn_pre_redist_up_kn(1,:,i_k,i_kn_high) = prob_down_b_o_kn_pre_redist_up_kn(1,:,i_k,i_kn_high) + f_high;
                
                prob_down_b_o_kn_pre_redist_up_kn(2:end,2,i_k,i_kn_low) = f_low;
                prob_down_b_o_kn_pre_redist_up_kn(2:end,2,i_k,i_kn_high) = prob_down_b_o_kn_pre_redist_up_kn(2:end,2,i_k,i_kn_high) + f_high;
            end
            kappa_down_k_t_b_o_up_kn = einsum('ijkl,jklm,no->injkm', kappa_down_k_b_o_up_kn_pre_redist, prob_down_b_o_kn_pre_redist_up_kn, ones(param.T, 1));
            
            redistribution = r_redist_adjusted;
            
        case {3, 4, 5}
            % Time-dependant redistribution
            n_down_t = sum(n_down_t_o, 2);
            
            if param.transition_mechanism == 3  % linear time redistribution, zero redistribution at tminre
                assert(n_down_t(param.tminre) < 1, 'Continuity issue: all commuters departing at tzero');  % Just in case
            end
            
            % Build the linear system equations. This is the same in every
            % iteration except for the last row
            if isempty(A_matrix)
                A_matrix = eye(param.T);
                
                switch param.transition_mechanism
                    case 3  % linear time redistribution, zero redistribution at tminre
                        A_matrix(1,end) = -1;

                        A_matrix(2:param.tminre-1,end) = -(param.tminre - param.t(2:param.tminre-1)) / (param.tminre - 1);

                        A_matrix(param.tminre+1:end-1,end) = -(param.t(param.tminre+1:end-1) - param.tminre) / (param.T - param.tminre);
                        
                    case 4  % linear time redistribution, r[1] = t1_tminre_ratio * r[tminre]
                        A_matrix(1:param.tminre-1,param.tminre) = -(param.t1_tminre_ratio * (param.tminre - param.t(1:param.tminre-1)) + param.t(1:param.tminre-1) - 1) / (param.tminre - 1);
                        
                        A_matrix(param.tminre+1:end,param.tminre) = -(param.t1_tminre_ratio * (param.t(param.tminre+1:end) - param.tminre) + param.T - param.t(param.tminre+1:end)) / (param.T - param.tminre);
                        
                        A_matrix(param.tminre,:) = [];
                        
                        A_matrix = [A_matrix; zeros(1, param.T)];
                        
                
                    case 5  % distribute more after t_peak
                        t_peak = round(param.tpeak);
                        post_pre_peak_departures_ratio = (param.alpha - param.beta) / (param.alpha + param.gamma);

                        for t = 1 : param.T - 1
                            if t == t_peak
                                A_matrix(t,t+1) = -post_pre_peak_departures_ratio;
                            else
                                A_matrix(t,t+1) = -1;
                            end
                        end
                        
                end
                
                b_vector = zeros(param.T, 1);
            end
            
            A_matrix(end,:) = n_down_t.';
            b_vector(end) = p_bar;
            
            r_down_t = A_matrix \ b_vector;
            r_down_t_low = floor(r_down_t);
            r_down_t_high = r_down_t_low + 1;
            
            saturation_adjustment_down_t = zeros(param.T, 1);
            r_down_t_adjusted = zeros(param.T, 1);
            
            prob_down_t_up_kn_pre_redist = prob_up_t_kn_pre_redist;
            for t = 1 : param.T
                if n_down_t(t) > 0
                    prob_down_t_up_kn_pre_redist(t,:) = prob_down_t_up_kn_pre_redist(t,:) / n_down_t(t);
                end
                
                for i = 1 : r_down_t_low(t)
                    saturation_adjustment_down_t(t) = saturation_adjustment_down_t(t) + prob_down_t_up_kn_pre_redist(t,sne_param.n_k-i) * i;
                end
                normalizer = sum(prob_down_t_up_kn_pre_redist(t,1:sne_param.n_k-r_down_t_high(t)));
                if normalizer > 0
                    r_down_t_adjusted(t) = (r_down_t(t) - saturation_adjustment_down_t(t)) / normalizer;
                end
            end
            
            r_down_t_adjusted_low = floor(r_down_t_adjusted);
            r_down_t_adjusted_high = r_down_t_adjusted_low + 1;
            f_low_down_t = r_down_t_adjusted_high - r_down_t_adjusted;
            f_high_down_t =  1 - f_low_down_t;
            
            prob_down_t_kn_pre_redist_up_kn = zeros(param.T, sne_param.n_k, sne_param.n_k);
            for i_k = 1 : sne_param.n_k
                i_k_down_t = i_k * ones(param.T,1);
                
                i_kn_low_down_t = min([i_k + r_down_t_adjusted_low, sne_param.n_k_down_t], [], 2);
                i_kn_high_down_t = min([i_k + r_down_t_adjusted_high, sne_param.n_k_down_t], [], 2);
                
                ind_low = sub2ind([param.T, sne_param.n_k, sne_param.n_k], param.t, i_k_down_t, i_kn_low_down_t);
                ind_high = sub2ind([param.T, sne_param.n_k, sne_param.n_k], param.t, i_k_down_t, i_kn_high_down_t);
                
                prob_down_t_kn_pre_redist_up_kn(ind_low) = f_low_down_t;
                prob_down_t_kn_pre_redist_up_kn(ind_high) = prob_down_t_kn_pre_redist_up_kn(ind_high) + f_high_down_t;
            end
            
            kappa_down_k_t_b_o_up_kn = einsum('ikln,jnm->ijklm', kappa_down_k_b_o_up_kn_pre_redist, prob_down_t_kn_pre_redist_up_kn);
            
            redistribution = r_down_t_adjusted;
            
    end
end