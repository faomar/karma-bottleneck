% Plot SNE departure time policy
function plot_sne_pi_t(fg, position, colormap, sne_pi_down_tstar_mu_alpha_u_k_up_t_b, param, sne_param, max_i_k)
    persistent sne_pi_t_plot
    if ~ishandle(fg)
        figure(fg);
        fig = gcf;
        fig.Position = position;
        sne_pi_t_plot = cell(param.n_tstar, param.n_mu, param.n_alpha, param.n_u);
        n_rows = param.n_tstar * param.n_mu * param.n_alpha;
        n_cols = param.n_u;
        i_subplot = 1;
        for i_tstar = 1 : param.n_tstar
            for i_mu = 1 : param.n_mu
                for i_alpha = 1 : param.n_alpha
                    for i_u = 1 : param.n_u
                        pi_mat = squeeze(sum(sne_pi_down_tstar_mu_alpha_u_k_up_t_b(i_tstar,i_mu,i_alpha,i_u,1:max_i_k,:,:), 7));

                        subplot(n_rows, n_cols, i_subplot);
                        sne_pi_t_plot{i_tstar,i_mu,i_alpha,i_u} = heatmap(param.t, sne_param.K(1:max_i_k), pi_mat, 'ColorbarVisible','off', 'MissingDataColor', [0.8275 0.8275 0.8275]);    % disallowed bids displayed gray
                        sne_pi_t_plot{i_tstar,i_mu,i_alpha,i_u}.YDisplayData = flipud(sne_pi_t_plot{i_tstar,i_mu,i_alpha,i_u}.YDisplayData);
                        
                        title_str = ['t*=', num2str(param.tstar(i_tstar))];
                        if param.n_mu > 1
                            title_str = [title_str, ' \mu=', num2str(i_mu)'];
                        end
                        if param.n_alpha > 1
                            alpha = param.Alpha(i_alpha);
                            if alpha > 0.99 && alpha < 1
                                alpha_str = num2str(alpha, '%.3f');
                            else
                                alpha_str = num2str(alpha, '%.2f');
                            end
                            title_str = [title_str, ' \alpha=', alpha_str];
                        end
                        sne_pi_t_plot{i_tstar,i_mu,i_alpha,i_u}.Title = [title_str, ' u=', num2str(param.U_down_mu(i_mu,i_u))];
                        
                        sne_pi_t_plot{i_tstar,i_mu,i_alpha,i_u}.XLabel = 'Departure time';
                        sne_pi_t_plot{i_tstar,i_mu,i_alpha,i_u}.YLabel = 'Karma';
                        
                        sne_pi_t_plot{i_tstar,i_mu,i_alpha,i_u}.FontName = 'ubuntu';
                        sne_pi_t_plot{i_tstar,i_mu,i_alpha,i_u}.FontSize = sne_param.plot_font_size;
                        
                        sne_pi_t_plot{i_tstar,i_mu,i_alpha,i_u}.Colormap = colormap;
                        sne_pi_t_plot{i_tstar,i_mu,i_alpha,i_u}.ColorLimits = [0 1];
                        sne_pi_t_plot{i_tstar,i_mu,i_alpha,i_u}.CellLabelColor = 'none';
                        
                        sne_pi_t_plot{i_tstar,i_mu,i_alpha,i_u}.GridVisible = false;
                        
                        for i_k = 1 : max_i_k
                            if mod(i_k - 1, 5) ~= 0
                                sne_pi_t_plot{i_tstar,i_mu,i_alpha,i_u}.YDisplayLabels{i_k} = '';
                            end
                        end

                        i_subplot = i_subplot + 1;
                    end
                end
            end
        end
        sgtitle('SNE departure time policy', 'FontSize', 20, 'FontName', 'ubunutu', 'FontWeight', 'bold');
    else
        for i_tstar = 1 : param.n_tstar
            for i_mu = 1 : param.n_mu
                for i_alpha = 1 : param.n_alpha
                    for i_u = 1 : param.n_u
                        pi_mat = squeeze(sum(sne_pi_down_tstar_mu_alpha_u_k_up_t_b(i_tstar,i_mu,i_alpha,i_u,1:max_i_k,:,:), 7));
                        sne_pi_t_plot{i_tstar,i_mu,i_alpha,i_u}.ColorData = pi_mat;
                        
                        title_str = ['t*=', num2str(param.tstar(i_tstar))];
                        if param.n_mu > 1
                            title_str = [title_str, ' \mu=', num2str(i_mu)'];
                        end
                        if param.n_alpha > 1
                            alpha = param.Alpha(i_alpha);
                            if alpha > 0.99 && alpha < 1
                                alpha_str = num2str(alpha, '%.3f');
                            else
                                alpha_str = num2str(alpha, '%.2f');
                            end
                            title_str = [title_str, ' \alpha=', alpha_str];
                        end
                        sne_pi_t_plot{i_tstar,i_mu,i_alpha,i_u}.Title = [title_str, ' u=', num2str(param.U_down_mu(i_mu,i_u))];
                    end
                end
            end
        end
    end
end