% Write traffic conditions to csv file
function write_traffic_csv(n_down_tstar_mu_alpha_u_t_o, n_down_t_o, q_down_t_o, t_q_down_t_o, t_analytical, U, r_analytical_down_u_t_o, r_analytical_down_t_o, q_analytical_down_t_o, t_q_analytical_down_t_o, param, sne_param, fileprefix)
    % Header for karma results
    header = ["tstar", "mu", "alpha", "i-u", "u", "t", "t-min", "o", "n-type-u", "N-type-u", "n-cum-type-u", "N-cum-type-u", "n-revcum-type-u", "N-revcum-type-u", "n", "N", "q", "Q", "t-q", "t-q-min"];
    filename = [fileprefix, '-traffic.csv'];
    fout = fopen(filename, 'w');
    for i = 1 : length(header) - 1
        fprintf(fout, '%s,', header(i));
    end
    fprintf(fout, '%s\n', header(end));
    fclose(fout);

    % Header for analytical tolling results
    header_analytical = ["u", "t-min", "o", "r-u", "R-u", "r-cum-u", "R-cum-u", "r-revcum-u", "R-revcum-u", "r-u-dt", "R-u-dt", "r-cum-u-dt", "R-cum-u-dt", "r-revcum-u-dt", "R-revcum-u-dt", "r", "R", "r-dt", "R-dt", "q", "Q", "t-q-min"];
    filename_analytical = [fileprefix, '-traffic-analytical.csv'];
    fout = fopen(filename_analytical, 'w');
    for i = 1 : length(header_analytical) - 1
        fprintf(fout, '%s,', header_analytical(i));
    end
    fprintf(fout, '%s\n', header_analytical(end));
    fclose(fout);

    % Cumulative departures/rates per type and urgency - used for
    % cumulative stem graphs
    n_cum_down_tstar_mu_alpha_u_t_o = zeros(size(n_down_tstar_mu_alpha_u_t_o));
    n_revcum_down_tstar_mu_alpha_u_t_o = zeros(size(n_down_tstar_mu_alpha_u_t_o));
    n_cum = zeros(param.T, sne_param.n_o);
    n_revcum = n_down_t_o;
    for i_tstar = 1 : param.n_tstar
        for i_mu = 1 : param.n_mu
            for i_alpha = 1 : param.n_alpha
                for i_u = 1 : param.n_u
                    n_cum = n_cum + squeeze(n_down_tstar_mu_alpha_u_t_o(i_tstar,i_mu,i_alpha,i_u,:,:));
                    n_cum_down_tstar_mu_alpha_u_t_o(i_tstar,i_mu,i_alpha,i_u,:,:) = n_cum;
                    
                    n_revcum_down_tstar_mu_alpha_u_t_o(i_tstar,i_mu,i_alpha,i_u,:,:) = n_revcum;
                    n_revcum = n_revcum - squeeze(n_down_tstar_mu_alpha_u_t_o(i_tstar,i_mu,i_alpha,i_u,:,:));
                end
            end
        end
    end
    
    r_cum_analytical_down_u_t_o = cumsum(r_analytical_down_u_t_o, 1);
    r_revcum_analytical_down_u_t_o = cumsum(r_analytical_down_u_t_o, 1, 'reverse');
    
    % Data for karma file
    for i_tstar = 1 : param.n_tstar
        tstar = param.tstar(i_tstar);
        for i_mu = 1 : param.n_mu
            for i_alpha = 1 : param.n_alpha
                alpha = param.Alpha(i_alpha);
                for i_u = 1 : param.n_u
                    u = param.U_down_mu(i_mu,i_u);
                    for i_o = 1 : sne_param.n_o
                        for t = 1 : param.T
                            line = [tstar, i_mu, alpha, i_u, u, t, param.t_min(t), sne_param.O(i_o), ...
                                n_down_tstar_mu_alpha_u_t_o(i_tstar,i_mu,i_alpha,i_u,t,i_o), n_down_tstar_mu_alpha_u_t_o(i_tstar,i_mu,i_alpha,i_u,t,i_o) * param.N, ...
                                n_cum_down_tstar_mu_alpha_u_t_o(i_tstar,i_mu,i_alpha,i_u,t,i_o), n_cum_down_tstar_mu_alpha_u_t_o(i_tstar,i_mu,i_alpha,i_u,t,i_o) * param.N, ...
                                n_revcum_down_tstar_mu_alpha_u_t_o(i_tstar,i_mu,i_alpha,i_u,t,i_o), n_revcum_down_tstar_mu_alpha_u_t_o(i_tstar,i_mu,i_alpha,i_u,t,i_o) * param.N, ...
                                n_down_t_o(t,i_o), n_down_t_o(t,i_o) * param.N, ...
                                q_down_t_o(t,i_o), q_down_t_o(t,i_o) * param.N, ...
                                t_q_down_t_o(t,i_o), t_q_down_t_o(t,i_o) * param.dt];
                            dlmwrite(filename, line, '-append');
                        end
                    end
                end
            end
        end
    end
    
    % Data for analytical tolling file
    for i_u = 1 : length(U)
        u_vec = U(i_u) * ones(length(t_analytical), 1);
        for i_o = 1 : sne_param.n_o
            o_vec = sne_param.O(i_o) * ones(length(t_analytical), 1);
            data = [u_vec, t_analytical, o_vec, ...
                reshape(r_analytical_down_u_t_o(i_u,:,i_o), [], 1), reshape(r_analytical_down_u_t_o(i_u,:,i_o), [], 1) * param.N, ...
                reshape(r_cum_analytical_down_u_t_o(i_u,:,i_o), [], 1), reshape(r_cum_analytical_down_u_t_o(i_u,:,i_o), [], 1) * param.N, ...
                reshape(r_revcum_analytical_down_u_t_o(i_u,:,i_o), [], 1), reshape(r_revcum_analytical_down_u_t_o(i_u,:,i_o), [], 1) * param.N, ...
                reshape(r_analytical_down_u_t_o(i_u,:,i_o), [], 1) * param.dt, reshape(r_analytical_down_u_t_o(i_u,:,i_o), [], 1) * param.dt * param.N, ...
                reshape(r_cum_analytical_down_u_t_o(i_u,:,i_o), [], 1) * param.dt, reshape(r_cum_analytical_down_u_t_o(i_u,:,i_o), [], 1) * param.dt * param.N, ...
                reshape(r_revcum_analytical_down_u_t_o(i_u,:,i_o), [], 1) * param.dt, reshape(r_revcum_analytical_down_u_t_o(i_u,:,i_o), [], 1) * param.dt * param.N, ...
                r_analytical_down_t_o(:,i_o), r_analytical_down_t_o(:,i_o) * param.N, ...
                r_analytical_down_t_o(:,i_o) * param.dt, r_analytical_down_t_o(:,i_o) * param.dt * param.N, ...
                q_analytical_down_t_o(:,i_o), q_analytical_down_t_o(:,i_o) * param.N, ...
                t_q_analytical_down_t_o(:,i_o)];
            dlmwrite(filename_analytical, data, '-append');
        end
    end
end