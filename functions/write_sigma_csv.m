% Write stationary karma distribution to csv file
function write_sigma_csv(sigma_down_tstar_mu_alpha_up_k, k_max, param, sne_param, fileprefix)
    % We put distribution of k >= k_max in k_max
    n_k = k_max + 1;
    sigma_down_tstar_mu_alpha_up_k(:,:,:,n_k) = sum(sigma_down_tstar_mu_alpha_up_k(:,:,:,n_k:end), 4);
    sigma_down_tstar_mu_alpha_up_k(:,:,:,n_k+1) = sigma_down_tstar_mu_alpha_up_k(:,:,:,n_k);
    
    % Header
    header = ["tstar", "mu", "alpha", "k", "k2", "P(k)"];
    filename = [fileprefix, '-karma-dist.csv'];
    fout = fopen(filename, 'w');
    for i = 1 : length(header) - 1
        fprintf(fout, '%s,', header(i));
    end
    fprintf(fout, '%s\n', header(end));
    fclose(fout);

    % Data
    for i_tstar = 1 : param.n_tstar
        tstar_vec = param.tstar(i_tstar) * ones(n_k + 1, 1);
        for i_mu = 1 : param.n_mu
            mu_vec = i_mu * ones(n_k + 1, 1);
            for i_alpha = 1 : param.n_alpha
                alpha_vec = param.Alpha(i_alpha) * ones(n_k + 1, 1);
                data = [tstar_vec, mu_vec, alpha_vec, sne_param.K(1:n_k+1), sne_param.K(1:n_k+1) - 0.5, squeeze(sigma_down_tstar_mu_alpha_up_k(i_tstar,i_mu,i_alpha,1:n_k+1))];
                dlmwrite(filename, data, '-append');
            end
        end
    end
end