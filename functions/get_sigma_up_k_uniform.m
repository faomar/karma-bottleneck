function sigma_up_k_uniform = get_sigma_up_k_uniform(k_bar, sne_param)
% Gets uniform distribution over karma adjusted to have correct
% average karma k_bar
    
    if k_bar * 2 <= sne_param.k_max
        i_k_bar2 = find(sne_param.K == k_bar * 2);
        sigma_up_k_uniform = [1 / i_k_bar2 * ones(i_k_bar2, 1); zeros(sne_param.n_k - i_k_bar2, 1)];

    elseif k_bar >= sne_param.k_max
        sigma_up_k_uniform = zeros(sne_param.n_k, 1);
        sigma_up_k_uniform(end) = 1;

    else
        sigma_up_k_uniform = 1 / sne_param.n_k * ones(sne_param.n_k, 1);
        K_small = 0 : k_bar - 1;
        K_big = k_bar + 1 : sne_param.k_max;
        num_K_small = length(K_small);
        num_K_big = length(K_big);
        delta_constant = sum(K_small) / num_K_small - sum(K_big) / num_K_big;
        delta_k_ave = k_bar - sne_param.K.' * sigma_up_k_uniform;
        delta_p = delta_k_ave / delta_constant;
        sigma_up_k_uniform(1:sne_param.i_k_bar-1) = sigma_up_k_uniform(1:sne_param.i_k_bar-1) + delta_p / num_K_small;
        sigma_up_k_uniform(sne_param.i_k_bar+1:end) = sigma_up_k_uniform(sne_param.i_k_bar+1:end) - delta_p / num_K_big;
    end


end