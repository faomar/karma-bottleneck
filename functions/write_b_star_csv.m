% Write b_star to csv file
function write_b_star_csv(b_star, p_bar, t_analytical, p_analytical_down_t_o, param, fileprefix)
    % Header for b_star
    header = ["t", "t-min", "b-star", "p-bar", "b-star-p-bar"];
    filename = [fileprefix, '-b-star.csv'];
    fout = fopen(filename, 'w');
    for i = 1 : length(header) - 1
        fprintf(fout, '%s,', header(i));
    end
    fprintf(fout, '%s\n', header(end));
    fclose(fout);

    % Header for analytical optimal toll
    header_analytical = ["t-min", "p-star"];
    filename_analytical = [fileprefix, '-p-star-analytical.csv'];
    fout = fopen(filename_analytical, 'w');
    for i = 1 : length(header_analytical) - 1
        fprintf(fout, '%s,', header_analytical(i));
    end
    fprintf(fout, '%s\n', header_analytical(end));
    fclose(fout);
    
    % Data for b-star
    data = [param.t, param.t_min, b_star, p_bar * ones(param.T, 1), b_star - p_bar];
    dlmwrite(filename, data, '-append');
    
    % Data for analytical optimal toll
    data = [t_analytical, p_analytical_down_t_o(:,1)];
    dlmwrite(filename_analytical, data, '-append');
end