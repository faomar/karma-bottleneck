clear all;
clc;
close all;

tic;

%% Add functions folder to path
addpath('functions');

%% Screen size used to place plots
screenwidth = 1920;
screenheight = 1080;
load('RedColormap.mat');

%% Parameters
% Setting parameters
param = load_params();

% SNE computation parameters
sne_param = load_sne_params(param);


%% Analytical traffic characteristics from bottleneck model
[t_analytical, r_analytical_down_t_o, q_analytical_down_t_o, t_q_analytical_down_t_o, U, prob_up_u, t_start_analytical_down_u, t_end_analytical_down_u, r_analytical_down_u_t_o, p_analytical_down_t_o, t_q_bar_analytical_down_u, c_bar_analytical_down_u] = get_queue_analytical(param, sne_param);


%% Evolutionary dynamics algorithm to find stationary Nash equilibrium %%
%% Initialization
% Initial policy
pi_down_tstar_mu_alpha_u_k_up_t_b_init = get_pi_init(param, sne_param);

% Initial type-state distribution
d_up_tstar_mu_alpha_u_k_init = get_d_init(param.k_bar, param, sne_param);


%% Pre-comuptations for speed
kappa_down_k_b_o_up_kn_pre_redist = get_kappa_pre_redist(param, sne_param);

%% Loop over alphas
i_alpha_comp = 1;
while i_alpha_comp <= param.n_alpha_comp
    if param.n_alpha_comp > 1
        fprintf('%%%%ALPHA = %.3f%%%%\n\n', param.Alpha(i_alpha_comp));
    end

    %% Initialization
    sne_pi_down_tstar_mu_alpha_u_k_up_t_b = pi_down_tstar_mu_alpha_u_k_up_t_b_init;
    sne_d_up_tstar_mu_alpha_u_k = d_up_tstar_mu_alpha_u_k_init;

    iter = 1;
    sne_pi_error = inf;
    sne_pi_error_hist = zeros(sne_param.max_iter, 1);
    sne_d_error = inf;
    sne_d_error_hist = zeros(sne_param.max_iter, 1);
    if sne_param.karma_initialization == 10
        load(sne_param.initialization_file, 'sne_V_down_tstar_mu_alpha_u_k');
    else
        sne_V_down_tstar_mu_alpha_u_k = zeros(param.n_tstar, param.n_mu, param.n_alpha, param.n_u, sne_param.n_k);
    end
    sne_V_down_tstar_mu_alpha_u_k_next = zeros(param.n_tstar, param.n_mu, param.n_alpha, param.n_u, sne_param.n_k);
    sne_Q_down_tstar_mu_alpha_u_k_t_b = zeros(param.n_tstar, param.n_mu, param.n_alpha, param.n_u, sne_param.n_k, param.T, sne_param.n_k);
    br_pi_down_tstar_mu_alpha_u_k_up_t_b = zeros(param.n_tstar, param.n_mu, param.n_alpha, param.n_u, sne_param.n_k, param.T, sne_param.n_k);
    k_max_saturated = false;

    %% History of policy and type-state distribution
    if sne_param.store_hist
        sne_pi_hist = cell(sne_param.max_iter + 1, 1);
        sne_pi_hist{1} = sne_pi_down_tstar_mu_alpha_u_k_up_t_b;
        sne_d_hist = cell(sne_param.max_iter + 1, 1);
        sne_d_hist{1} = sne_d_up_tstar_mu_alpha_u_k;
        sne_pi_error_to_eq_hist = zeros(sne_param.max_iter + 1, 1);
        sne_d_error_to_eq_hist = zeros(sne_param.max_iter + 1, 1);
    end

    
    %% Main loop of algorithm
    while (sne_pi_error > sne_param.sne_pi_tol || sne_d_error > sne_param.sne_d_tol) && iter <= sne_param.max_iter

        %% Pre-processing 
        % Calculate mass of bids and probability of accessing fast lane
        
        % Mass of karma-departure time-bids
        prob_up_k_t_b = einsum('ijklm,ijklmno->mno', sne_d_up_tstar_mu_alpha_u_k, sne_pi_down_tstar_mu_alpha_u_k_up_t_b);

        % Mass of departure times and bids
        sne_nu_up_t_b = squeeze(sum(prob_up_k_t_b, 1));

         % Probability of accessing fast/slow lane
        [psi_down_t_b_up_o, b_star] = get_psi(sne_nu_up_t_b, param, sne_param);
        
        % Plot
        if sne_param.plot
            sne_pi_fig = 1;
            sne_pi_fig_pos = [0, 0, screenwidth, screenheight];
            plot_sne_pi(sne_pi_fig, sne_pi_fig_pos, RedColormap, sne_pi_down_tstar_mu_alpha_u_k_up_t_b, param, sne_param, b_star, sne_param.n_k);

            sne_d_fig = 2;
            sne_d_fig_pos = [0, 0, screenwidth, screenheight];
            plot_sne_d(sne_d_fig, sne_d_fig_pos, sne_d_up_tstar_mu_alpha_u_k, param, sne_param);
            
            sne_nu_fig = 3;
            sne_nu_fig_pos = [0, 0, screenwidth, screenheight];
            plot_sne_nu(sne_nu_fig, sne_nu_fig_pos, sne_nu_up_t_b, param, sne_param, b_star);
        end
        
        
        %% Queuing time in slow lane
        [n_down_tstar_mu_alpha_u_t_o, n_down_t_o, q_down_t_o, t_q_down_t_o] = get_queue(sne_pi_down_tstar_mu_alpha_u_k_up_t_b, sne_d_up_tstar_mu_alpha_u_k, sne_nu_up_t_b, psi_down_t_b_up_o, param, sne_param);    
        
        
        %% Immediate reward function
        zeta_down_tstar_mu_u_t_b = get_immediate_reward(t_q_down_t_o, psi_down_t_b_up_o, param, sne_param);

        
        %% Karma transition function
        [kappa_down_k_t_b_o_up_kn, redistribution] = get_kappa(kappa_down_k_b_o_up_kn_pre_redist, prob_up_k_t_b, sne_nu_up_t_b, psi_down_t_b_up_o, n_down_t_o, param, sne_param);
        
        
        %% State transition function
        prob_down_k_t_b_up_kn = einsum('ijkml,jkm->ijkl', kappa_down_k_t_b_o_up_kn, psi_down_t_b_up_o);
        rho_down_mu_u_k_t_b_up_un_kn = einsum('ijn,klmo->ijklmno', param.phi_down_mu_u_up_un, prob_down_k_t_b_up_kn);


        %% Expected Immediate Reward
        sne_R_down_tstar_mu_alpha_u_k = einsum('ijklmno,ijlno->ijklm', sne_pi_down_tstar_mu_alpha_u_k_up_t_b, zeta_down_tstar_mu_u_t_b);


        %% State Transition Probabilities
        sne_P_down_tstar_mu_alpha_u_k_up_un_kn = einsum('ijklmpq,jlmpqno->ijklmno', sne_pi_down_tstar_mu_alpha_u_k_up_t_b, rho_down_mu_u_k_t_b_up_un_kn);


        %% Value Function -> Expected Infinite Horizon Reward
        % For each alpha value/type
        for i_alpha = 1 : param.n_alpha
            alpha = param.Alpha(max([i_alpha, i_alpha_comp]));
            V_iter = 1;
            V_error = inf;
            while V_error > sne_param.V_tol && iter <= sne_param.V_max_iter
                if alpha == 1 % average cost per stage problem
                    for i_tstar = 1 : param.n_tstar
                        for i_mu = 1 : param.n_mu
                            rel_V = sne_R_down_tstar_mu_alpha_u_k(i_tstar,i_mu,i_alpha,1,sne_param.i_k_bar) + einsum('ijklmno,ijkno->ijklm', sne_P_down_tstar_mu_alpha_u_k_up_un_kn(i_tstar,i_mu,i_alpha,1,sne_param.i_k_bar,:,:), sne_V_down_tstar_mu_alpha_u_k(i_tstar,i_mu,i_alpha,:,:));
                            sne_V_down_tstar_mu_alpha_u_k_next(i_tstar,i_mu,i_alpha,:,:) = sne_R_down_tstar_mu_alpha_u_k(i_tstar,i_mu,i_alpha,:,:) + einsum('ijklmno,ijkno->ijklm', sne_P_down_tstar_mu_alpha_u_k_up_un_kn(i_tstar,i_mu,i_alpha,:,:,:,:), sne_V_down_tstar_mu_alpha_u_k(i_tstar,i_mu,i_alpha,:,:)) - rel_V;
                        end
                    end
                    sne_V_down_tstar_mu_alpha_u_k_next = sne_V_down_tstar_mu_alpha_u_k;
                else
                    sne_V_down_tstar_mu_alpha_u_k_next(:,:,i_alpha,:,:) = sne_R_down_tstar_mu_alpha_u_k(:,:,i_alpha,:,:) + alpha * einsum('ijklmno,ijkno->ijklm', sne_P_down_tstar_mu_alpha_u_k_up_un_kn(:,:,i_alpha,:,:,:,:), sne_V_down_tstar_mu_alpha_u_k(:,:,i_alpha,:,:));
                end
                V_error = norm(reshape(sne_V_down_tstar_mu_alpha_u_k(:,:,i_alpha,:,:) - sne_V_down_tstar_mu_alpha_u_k_next(:,:,i_alpha,:,:), 1, []), inf);
                sne_V_down_tstar_mu_alpha_u_k(:,:,i_alpha,:,:) = sne_V_down_tstar_mu_alpha_u_k_next(:,:,i_alpha,:,:);
                V_iter = V_iter + 1;

            end
            if V_iter > sne_param.V_max_iter
                fprintf('\nWARNING: Bellman recursion did not converge! Error: %f\n\n', V_error);
            end
        end

        
        %% Single-stage deviation reward
        r_down_tstar_mu_alpha_u_k_t_b = einsum('ijlno,km->ijklmno', zeta_down_tstar_mu_u_t_b, ones(param.n_alpha, sne_param.n_k));
        future_r_down_tstar_mu_alpha_u_k_t_b = einsum('jlmnopq,ijkpq->ijklmno', rho_down_mu_u_k_t_b_up_un_kn, sne_V_down_tstar_mu_alpha_u_k);
        for i_alpha = 1 : param.n_alpha
            alpha = param.Alpha(max([i_alpha, i_alpha_comp]));
            sne_Q_down_tstar_mu_alpha_u_k_t_b(:,:,i_alpha,:,:) = r_down_tstar_mu_alpha_u_k_t_b(:,:,i_alpha,:,:) + alpha * future_r_down_tstar_mu_alpha_u_k_t_b(:,:,i_alpha,:,:);
        end


        %% Perturbed best response policy
%         lambda_Q_down_tstar_mu_alpha_u_k_t_b = sne_param.lambda * sne_Q_down_tstar_mu_alpha_u_k_t_b;
        lambda_Q_down_tstar_mu_alpha_u_k_t_b = einsum('ijklmno,jl->ijklmno', sne_Q_down_tstar_mu_alpha_u_k_t_b, sne_param.lambda ./ param.U_down_mu);
        for i_k = 1 : sne_param.n_k
            br_pi_down_tstar_mu_alpha_u_k_up_t_b(:,:,:,:,i_k,:,1:i_k) = exp(lambda_Q_down_tstar_mu_alpha_u_k_t_b(:,:,:,:,i_k,:,1:i_k) - max(lambda_Q_down_tstar_mu_alpha_u_k_t_b(:,:,:,:,i_k,:,1:i_k), [], [6 7])); % Subtract max for numerical stability
            assert(max(br_pi_down_tstar_mu_alpha_u_k_up_t_b(:)) < inf, 'Numerics exploding - decrease lambda');  % Just in case
        end
        br_pi_down_tstar_mu_alpha_u_k_up_t_b = br_pi_down_tstar_mu_alpha_u_k_up_t_b ./ sum(br_pi_down_tstar_mu_alpha_u_k_up_t_b, [6 7]);
        
%         % Only update policy if there is significant imporvement (i.e.,
%         % epsilon-Nash equilibrium)
%         br_V_down_tstar_mu_alpha_u_k = einsum('ijklmno,ijklmno->ijklm', br_pi_down_tstar_mu_alpha_u_k_up_t_b, sne_Q_down_tstar_mu_alpha_u_k_t_b);
%         i_insignificant_improvement = find((br_V_down_tstar_mu_alpha_u_k - sne_V_down_tstar_mu_alpha_u_k) ./ abs(sne_V_down_tstar_mu_alpha_u_k) < sne_param.br_Q_tol);
%         if ~isempty(i_insignificant_improvement)
%             [i_tstar, i_mu, i_alpha, i_u, i_k] = ind2sub([param.n_tstar, param.n_mu, param.n_alpha, param.n_u, sne_param.n_k], i_insignificant_improvement);
%             for i_row = 1 : length(i_tstar)
%                 br_pi_down_tstar_mu_alpha_u_k_up_t_b(i_tstar(i_row),i_mu(i_row),i_alpha(i_row),i_u(i_row),i_k(i_row),:,:) = sne_pi_down_tstar_mu_alpha_u_k_up_t_b(i_tstar(i_row),i_mu(i_row),i_alpha(i_row),i_u(i_row),i_k(i_row),:,:);
%             end
%         end

        % Plot
        if sne_param.plot
            br_pi_fig = 4;
            br_pi_fig_pos = [0, 0, screenwidth, screenheight];
            plot_br_pi(br_pi_fig, br_pi_fig_pos, RedColormap, br_pi_down_tstar_mu_alpha_u_k_up_t_b, param, sne_param, b_star);
            
            drawnow;
        end


        %% Next policy
        sne_pi_down_tstar_mu_alpha_u_k_up_t_b_next = (1 - sne_param.eta * sne_param.dt) * sne_pi_down_tstar_mu_alpha_u_k_up_t_b + sne_param.eta * sne_param.dt * br_pi_down_tstar_mu_alpha_u_k_up_t_b;
        sne_pi_diff = sne_pi_down_tstar_mu_alpha_u_k_up_t_b_next - sne_pi_down_tstar_mu_alpha_u_k_up_t_b;
        sne_pi_error = norm(reshape(sne_pi_diff, 1, []), inf);


        %% Next type-state distribution
        sne_d_up_tstar_mu_alpha_u_k_next = einsum('ijklm,ijklmno->ijkno', sne_d_up_tstar_mu_alpha_u_k, sne_P_down_tstar_mu_alpha_u_k_up_un_kn);
        sne_d_up_tstar_mu_alpha_u_k_next = sne_d_up_tstar_mu_alpha_u_k_next / sum(sne_d_up_tstar_mu_alpha_u_k_next(:));
        sne_d_up_tstar_mu_alpha_u_k_next = (1 - sne_param.dt) * sne_d_up_tstar_mu_alpha_u_k + sne_param.dt * sne_d_up_tstar_mu_alpha_u_k_next;

        % Detect if karma is saturating at current k_max
        sigma_up_k = squeeze(sum(sne_d_up_tstar_mu_alpha_u_k_next, [1 2 3 4]));
        assert(sigma_up_k(end) <= sne_param.max_sigma_k_max, 'Too much saturation - increase k_max');

        sne_d_error = norm(reshape(sne_d_up_tstar_mu_alpha_u_k_next - sne_d_up_tstar_mu_alpha_u_k, 1, []), inf);


        
        %% Update SNE policy & distribution candidates
        sne_pi_down_tstar_mu_alpha_u_k_up_t_b = sne_pi_down_tstar_mu_alpha_u_k_up_t_b_next;
        sne_d_up_tstar_mu_alpha_u_k = sne_d_up_tstar_mu_alpha_u_k_next;

        %% Finish up iteration
        % Display status
        fprintf('Iteration %d policy error %f distribution error %f\n', iter, sne_pi_error, sne_d_error);
        
        % Update history
        sne_pi_error_hist(iter) = sne_pi_error;
        sne_d_error_hist(iter) = sne_d_error;

        if sne_param.store_hist
            sne_pi_hist{iter+1} = sne_pi_down_tstar_mu_alpha_u_k_up_t_b;
            sne_d_hist{iter+1} = sne_d_up_tstar_mu_alpha_u_k;
        end
        
        % Increment iteration count
        iter = iter + 1;
    end

    %% Remove 'extra' history
    sne_pi_error_hist(iter:end) = [];
    sne_d_error_hist(iter:end) = [];

    %% Difference with eq policy
    if sne_param.store_hist
        for i_iter = 1 : iter - 1
            sne_pi_diff_to_eq = sne_pi_down_tstar_mu_alpha_u_k_up_t_b - sne_pi_hist{i_iter};
            sne_pi_error_to_eq_hist(i_iter) = norm(reshape(sne_pi_diff_to_eq, 1, []));
            
            sne_d_diff_to_eq = sne_d_up_tstar_mu_alpha_u_k - sne_d_hist{i_iter};
            sne_d_error_to_eq_hist(i_iter) = norm(reshape(sne_d_diff_to_eq, 1, []));
        end
    end

    %% Some final results
    % Marginal karma distribution
    sne_sigma_up_k = squeeze(sum(sne_d_up_tstar_mu_alpha_u_k, [1 2 3 4]));
    sne_sigma_down_tstar_mu_alpha_up_k = zeros(param.n_tstar, param.n_mu, param.n_alpha, sne_param.n_k);
    for i_tstar = 1 : param.n_tstar
        for i_mu = 1 : param.n_mu
            for i_alpha = 1 : param.n_alpha
                sne_sigma_down_tstar_mu_alpha_up_k(i_tstar,i_mu,i_alpha,:) = squeeze(sum(sne_d_up_tstar_mu_alpha_u_k(i_tstar,i_mu,i_alpha,:,:), 4));
                sne_sigma_down_tstar_mu_alpha_up_k(i_tstar,i_mu,i_alpha,:) = sne_sigma_down_tstar_mu_alpha_up_k(i_tstar,i_mu,i_alpha,:) / sum(sne_sigma_down_tstar_mu_alpha_up_k(i_tstar,i_mu,i_alpha,:), 4);
            end
        end
    end
    fprintf('\nAverage karma: %f\n', dot(sne_sigma_up_k, sne_param.K));

    %% Performance metrics
    % Average waiting time, per type
    t_q_bar_down_tstar_mu_alpha = einsum('ijklm,ijklmno,nop,np->ijk', sne_d_up_tstar_mu_alpha_u_k, sne_pi_down_tstar_mu_alpha_u_k_up_t_b, psi_down_t_b_up_o, t_q_down_t_o) ./ param.g_up_tstar_mu_alpha;
    
    % Average waiting time, system-wide
    t_q_bar = einsum('ijk,ijk->k', n_down_t_o, t_q_down_t_o);

    % Average reward, per type
    r_bar_down_tstar_mu_alpha = einsum('ijklm,ijklm->ijk', sne_d_up_tstar_mu_alpha_u_k, sne_R_down_tstar_mu_alpha_u_k) ./ param.g_up_tstar_mu_alpha;

    % Average reward, system-wide
    r_bar = einsum('ijkl,ijkl->l', param.g_up_tstar_mu_alpha, r_bar_down_tstar_mu_alpha);

    %% For alpha = 1: check that identical average stage cost assumption is satisfied
    % (see Bertsekas, DPOC Vol II, Chapter 4)
    for i_alpha = 1 : param.n_alpha
        alpha = param.Alpha(max([i_alpha, i_alpha_comp]));
        if alpha == 1
            V_check_down_tstar_mu_alpha_u_k = zeros(param.n_tstar, param.n_mu, param.n_alpha, param.n_u, sne_param.n_k);
            for i_tstar = 1 : param.n_tstar
                for i_mu = 1 : param.n_mu
                    V_check_down_tstar_mu_alpha_u_k(i_tstar,i_mu,i_alpha,:,:) = sne_R_down_tstar_mu_alpha_u_k(i_tstar,i_mu,i_alpha,:,:) + einsum('ijklmno,ijkno->ijklm', sne_P_down_tstar_mu_alpha_u_k_up_un_kn(i_tstar,i_mu,i_alpha,:,:,:,:), sne_V_down_tstar_mu_alpha_u_k(i_tstar,i_mu,i_alpha,:,:)) - sne_V_down_tstar_mu_alpha_u_k(i_tstar,i_mu,i_alpha,:,:);
                    V_max_dev = max(V_check_down_tstar_mu_alpha_u_k(i_tstar,i_mu,i_alpha,:,:), [], 'all') - min(V_check_down_tstar_mu_alpha_u_k(i_tstar,i_mu,i_alpha,:,:), [], 'all');
                    if V_max_dev > sne_param.V_tol
                        fprintf('\nWARNING: Identical average stage cost assumption unsatisfied! Max deviation: %f\n\n', V_max_dev);
                    end
                end
            end
        end
    end


    %% Plot remaining statistics
    if sne_param.plot
        % SNE karma distribution plot
        sne_sigma_fig = 5;
        sne_sigma_fig_pos = [0, 0, screenwidth, screenheight];
        plot_sne_sigma(sne_sigma_fig, sne_sigma_fig_pos, sne_sigma_down_tstar_mu_alpha_up_k, param, sne_param);
        
        % SNE value function plot
        sne_V_fig = 6;
        sne_V_fig_pos = [0, 0, screenwidth, screenheight];
        plot_sne_V(sne_V_fig, sne_V_fig_pos, sne_V_down_tstar_mu_alpha_u_k, param, sne_param);

        % SNE policy error plot
        sne_pi_d_error_fig = 7;
        sne_pi_d_error_fig_pos = [0, 0, screenwidth, screenheight];
        plot_sne_pi_error(sne_pi_d_error_fig, sne_pi_d_error_fig_pos, sne_pi_error_hist, sne_d_error_hist);
        
        % SNE policy & distribution error wrt equilibirum plot
        if sne_param.store_hist
            sne_pi_d_error_to_eq_fig = 8;
            sne_pi_d_error_to_eq_fig_pos = [0, 0, screenwidth, screenheight];
            plot_sne_pi_d_error(sne_pi_d_error_to_eq_fig, sne_pi_d_error_to_eq_fig_pos, sne_pi_error_to_eq_hist, sne_d_error_to_eq_hist);
        end
    end
    
    % Store end results
    if sne_param.save
        if ~exist('results', 'dir')
           mkdir('results');
        end
        
        file_str = ['results/k_bar-', num2str(param.k_bar, '%02d')];
        
        if param.n_tstar > 1
            file_str = [file_str, '-g_tstar'];
            for i_tstar = 1 : param.n_tstar
                file_str = [file_str, '-', num2str(param.g_up_tstar(i_tstar), '%.2f')];
            end
        end
        
        if param.n_mu > 1
            file_str = [file_str, '-g_mu'];
            for i_mu = 1 : param.n_mu
                file_str = [file_str, '-', num2str(param.g_up_mu(i_mu), '%.2f')];
            end
        end
        
        if param.n_alpha > 1
            file_str = [file_str, '-g_alpha'];
            for i_alpha = 1 : param.n_alpha
                file_str = [file_str, '-', num2str(param.g_up_alpha(i_alpha), '%.2f')];
            end
        else
            if alpha > 0.99 && alpha < 1
                alpha_str = num2str(alpha, '%.3f');
            else
                alpha_str = num2str(alpha, '%.2f');
            end
            file_str = [file_str, '-alpha-', alpha_str];
        end
        
        file_str = [file_str, '.mat'];
        save(file_str);
    end
    
    i_alpha_comp = i_alpha_comp + 1;

end
i_alpha_comp = i_alpha_comp - 1;


%% If plotting is not active, plot everything at the end
if ~sne_param.plot
    % SNE policy plot
    sne_pi_fig = 1;
    sne_pi_fig_pos = [0, 0, screenwidth, screenheight];
    plot_sne_pi(sne_pi_fig, sne_pi_fig_pos, RedColormap, sne_pi_down_tstar_mu_alpha_u_k_up_t_b, param, sne_param, b_star, sne_param.n_k);
%     plot_sne_pi(sne_pi_fig, sne_pi_fig_pos, RedColormap, sne_pi_down_tstar_mu_alpha_u_k_up_t_b, param, sne_param, b_star, 35);

    % SNE departure time policy plot
    sne_pi_t_fig = 2;
    sne_pi_t_fig_pos = [0, 0, screenwidth, screenheight];
    plot_sne_pi_t(sne_pi_t_fig, sne_pi_t_fig_pos, RedColormap, sne_pi_down_tstar_mu_alpha_u_k_up_t_b, param, sne_param, sne_param.n_k);
%     plot_sne_pi_t(sne_pi_t_fig, sne_pi_t_fig_pos, RedColormap, sne_pi_down_tstar_mu_alpha_u_k_up_t_b, param, sne_param, 35);
    
    % SNE stationary distribution plot
    sne_d_fig = 3;
    sne_d_fig_pos = [0, 0, screenwidth, screenheight];
    plot_sne_d(sne_d_fig, sne_d_fig_pos, sne_d_up_tstar_mu_alpha_u_k, param, sne_param);

    % SNE distribution of bids plot
    sne_nu_fig = 4;
    sne_nu_fig_pos = [0, 0, screenwidth, screenheight];
    plot_sne_nu(sne_nu_fig, sne_nu_fig_pos, sne_nu_up_t_b, param, sne_param, b_star);

    % Best response policy plot
    br_pi_fig = 5;
    br_pi_fig_pos = [0, 0, screenwidth, screenheight];
    plot_br_pi(br_pi_fig, br_pi_fig_pos, RedColormap, br_pi_down_tstar_mu_alpha_u_k_up_t_b, param, sne_param, b_star);

    % SNE karma distribution plot
    sne_sigma_fig = 6;
    sne_sigma_fig_pos = [0, 0, screenwidth, screenheight];
    plot_sne_sigma(sne_sigma_fig, sne_sigma_fig_pos, sne_sigma_down_tstar_mu_alpha_up_k, param, sne_param);
    
    % SNE traffic condition plot
    traffic_fig = 7;
    traffic_fig_pos = [0, 0, screenwidth, screenheight];
    plot_traffic(traffic_fig, traffic_fig_pos, n_down_tstar_mu_alpha_u_t_o, n_down_t_o, t_q_down_t_o, t_analytical, U, r_analytical_down_u_t_o, r_analytical_down_t_o, t_q_analytical_down_t_o, param, sne_param);

    % SNE value function plot
    sne_V_fig = 8;
    sne_V_fig_pos = [0, 0, screenwidth, screenheight];
    plot_sne_V(sne_V_fig, sne_V_fig_pos, sne_V_down_tstar_mu_alpha_u_k, param, sne_param);
    
    % SNE policy & distribution error plot
    sne_pi_d_error_fig = 9;
    sne_pi_d_error_fig_pos = [0, 0, screenwidth, screenheight];
    plot_sne_pi_d_error(sne_pi_d_error_fig, sne_pi_d_error_fig_pos, sne_pi_error_hist, sne_d_error_hist, sne_param);
    
    % SNE policy & distribution error wrt equilibirum plot
    if sne_param.store_hist
        sne_pi_d_error_to_eq_fig = 10;
        sne_pi_d_error_to_eq_fig_pos = [0, 0, screenwidth, screenheight];
        plot_sne_pi_d_error_to_eq(sne_pi_d_error_to_eq_fig, sne_pi_d_error_to_eq_fig_pos, sne_pi_error_to_eq_hist, sne_d_error_to_eq_hist, sne_param);
    end
end

% Save figures
if sne_param.save
    if ~exist('results/figures', 'dir')
       mkdir('results/figures');
    end
        
    file_start = 'results/figures/';
    file_end = ['-k_bar-', num2str(param.k_bar, '%02d')];
    if param.n_alpha == 1
        file_end = [file_end, '-alpha-', alpha_str];
    end
    file_end = [file_end,  '.svg'];
    saveas(sne_pi_fig, [file_start, '1-SNE-policy', file_end]);
    saveas(sne_pi_t_fig, [file_start, '2-SNE-t-policy', file_end]);
    saveas(sne_d_fig, [file_start, '3-SNE-dist', file_end]);
    saveas(sne_nu_fig, [file_start, '4-bid-dist', file_end]);
    saveas(br_pi_fig, [file_start, '5-BR-policy', file_end]);
    saveas(sne_sigma_fig, [file_start, '6-karma-dist', file_end]);
    saveas(traffic_fig, [file_start, '7-traffic', file_end]);
    saveas(sne_V_fig, [file_start, '8-value', file_end]);
    saveas(sne_pi_d_error_fig, [file_start, '9-policy-dist-error', file_end]);
    if sne_param.store_hist
        saveas(sne_pi_d_error_to_eq_fig, [file_start, '10-policy-dist-error-to-eq', file_end]);
    end
end

%% Inform user when done
fprintf('DONE\n\n');

toc;