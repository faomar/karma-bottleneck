clc;
close all;

%% Screen size used to place plots
screenwidth = 1920;
screenheight = 1080;

%% Get pre-computed results
% load('results/baseline-karma/k_bar-10-alpha-0.99.mat');

%% System-wide average waiting time
% 1 -> karma model
% 2 -> tolling analytical
% 3 -> no karma analytical
% 4 -> karma model processed
t_q_bar_min = zeros(4, 1);

% Analytical equilibrium cost (not weighted with urgency)
c_star = param.beta * param.gamma / ((param.beta + param.gamma) * param.s);

% Karma model
t_q_bar_min(1) = param.dt * t_q_bar;

% Market analytical
t_q_bar_min(2) = param.dt * param.s_2 / param.s * c_star / (2 * param.alpha);

% No karma analytical
t_q_bar_min(3) = param.dt * c_star / (2 * param.alpha);

% Karma model processed
% Counter-acts effect of time discretization by averaging queuing delays
% over 3 consecutive timesteps
t_q_processed_down_t_o = t_q_down_t_o;
t_peak = param.tstar - param.beta * param.gamma / (param.alpha * (param.beta + param.gamma) * param.s);
for t = 2 : param.T
%     if t ~= t_peak
        t_q_processed_down_t_o(t,2) = mean(squeeze(t_q_down_t_o(t-1:t,2)));
%     else
%         t_q_processed_down_t_o(t,2) = ((param.alpha + param.gamma) * mean(squeeze(t_q_down_t_o(t-1:t,2))) ...
%             + (param.alpha - param.beta) * mean(squeeze(t_q_down_t_o(t:t+1,2)))) ...
%             / (2 * param.alpha + param.gamma - param.beta);
%     end
end
t_q_bar_processed = einsum('ijk,ijk->k', n_down_t_o, t_q_processed_down_t_o);
t_q_bar_min(4) = param.dt * t_q_bar_processed;

%% System-wide average cost (without monetary charge)
% 1 -> karma model
% 2 -> tolling analytical
% 3 -> no karma analytical
c_bar = zeros(3, 1);

% Average urgency
u_bar = einsum('ik,ij,ij->k', param.g_up_mu, param.prob_down_mu_up_u, param.U_down_mu);

% Karma model
c_bar(1) = -r_bar;

% Market analytical
c_bar(2) = dot(prob_up_u, c_bar_analytical_down_u);

% No karma analytical
c_bar(3) = u_bar * c_star;

%% Plot
x_axis = categorical(["Karma"; "Toll"; "No split"; "Karma+"]);
x_axis = reordercats(x_axis, string(x_axis));
font_size = 15;
figure;
fig = gcf;
fig.Position = [0, 0, screenwidth, screenheight];

% System-wide average waiting time plot
subplot(1,2,1);
bar(x_axis(1), t_q_bar_min(1));
hold on;
bar(x_axis(2), t_q_bar_min(2));
bar(x_axis(3), t_q_bar_min(3));
bar(x_axis(4), t_q_bar_min(4));

axes = gca;

axes.Title.String = 'System-wide average waiting time [min]';

axes.Title.FontName = 'ubuntu';
axes.XAxis.FontName = 'ubuntu';
axes.YAxis.FontName = 'ubuntu';
lgd.FontName = 'ubuntu';

axes.Title.FontSize = font_size;
axes.XAxis.FontSize = font_size;
axes.YAxis.FontSize = font_size;
lgd.FontSize = font_size;

% System-wide average cost plot
subplot(1,2,2);
bar(x_axis(1), c_bar(1));
hold on;
bar(x_axis(2), c_bar(2));
bar(x_axis(3), c_bar(3));

axis tight;
axes = gca;

axes.Title.String = 'System-wide average cost';

axes.Title.FontName = 'ubuntu';
axes.XAxis.FontName = 'ubuntu';
axes.YAxis.FontName = 'ubuntu';
lgd.FontName = 'ubuntu';

axes.Title.FontSize = font_size;
axes.XAxis.FontSize = font_size;
axes.YAxis.FontSize = font_size;
lgd.FontSize = font_size;

sgtitle('Performance comparison', 'FontSize', 20, 'FontName', 'ubunutu', 'FontWeight', 'bold');

%% Waiting time per urgency type
% 1 -> karma model
% 2 -> tolling analytical
% 3 -> no karma analytical
% 4 -> karma model processed
t_q_bar_min_down_mu = zeros(4, param.n_mu);

% Karma model
t_q_bar_min_down_mu(1,:) = param.dt * t_q_bar_down_tstar_mu_alpha(1,:,1);

% Market analytical
for i_mu = 1 : param.n_mu
    for i_u = 1 : param.n_u
        i_u2 = find(U == param.U_down_mu(i_mu,i_u));
        
        t_q_bar_min_down_mu(2,i_mu) = t_q_bar_min_down_mu(2,i_mu) + ...
            param.prob_down_mu_up_u(i_mu,i_u) * t_q_bar_analytical_down_u(i_u2);
    end
end

% No karma analytical
t_q_bar_min_down_mu(3,:) = param.dt * c_star / (2 * param.alpha);

% Karma model processed
% Counter-acts effect of time discretization by averaging queuing delays
% over 3 consecutive timesteps
t_q_bar_processed_down_tstar_mu_alpha = einsum('ijklm,ijklmno,nop,np->ijk', sne_d_up_tstar_mu_alpha_u_k, sne_pi_down_tstar_mu_alpha_u_k_up_t_b, psi_down_t_b_up_o, t_q_processed_down_t_o) ./ param.g_up_tstar_mu_alpha;
t_q_bar_min_down_mu(4,:) = param.dt * t_q_bar_processed_down_tstar_mu_alpha(1,:,1);

%% Average cost per urgency type
% 1 -> karma model
% 2 -> tolling analytical
% 3 -> no karma analytical
c_bar_down_mu = zeros(3, param.n_mu);

% Karma model
c_bar_down_mu(1,:) = -r_bar_down_tstar_mu_alpha(1,:,1);

% Market analytical
for i_mu = 1 : param.n_mu
    for i_u = 1 : param.n_u
        i_u2 = find(U == param.U_down_mu(i_mu,i_u));
        
        c_bar_down_mu(2,i_mu) = c_bar_down_mu(2,i_mu) + ...
            param.prob_down_mu_up_u(i_mu,i_u) * c_bar_analytical_down_u(i_u2);
    end
end

% No karma analytical
c_bar_down_mu(3,:) = param.u_bar_down_mu * c_star;

%% Average cost per urgency type, normalized by average urgency of type
c_bar_norm_down_mu = c_bar_down_mu ./ reshape(param.u_bar_down_mu, 1, []);

%% Plot
x_axis = [];
for i_mu = 1 : param.n_mu
    x_axis = [x_axis; strcat("Karma \mu-", num2str(i_mu))];
end
for i_mu = 1 : param.n_mu
    x_axis = [x_axis; strcat("Toll \mu-", num2str(i_mu))];
end
for i_mu = 1 : param.n_mu
    x_axis = [x_axis; strcat("No split \mu-", num2str(i_mu))];
end
for i_mu = 1 : param.n_mu
    x_axis = [x_axis; strcat("Karma+ \mu-", num2str(i_mu))];
end
x_axis = categorical(x_axis);
x_axis = reordercats(x_axis, string(x_axis));
font_size = 15;
figure;
fig = gcf;
fig.Position = [0, 0, screenwidth, screenheight];

% Average waiting time per urgency type plot
subplot(1,2,1);
bar(x_axis(1:param.n_mu), reshape(t_q_bar_min_down_mu(1,:).', [], 1));
hold on;
bar(x_axis(param.n_mu+1:2*param.n_mu), reshape(t_q_bar_min_down_mu(2,:).', [], 1));
bar(x_axis(2*param.n_mu+1:3*param.n_mu), reshape(t_q_bar_min_down_mu(3,:).', [], 1));
bar(x_axis(3*param.n_mu+1:4*param.n_mu), reshape(t_q_bar_min_down_mu(4,:).', [], 1));

axes = gca;

axes.Title.String = 'Average waiting time per urgency type [min]';

axes.Title.FontName = 'ubuntu';
axes.XAxis.FontName = 'ubuntu';
axes.YAxis.FontName = 'ubuntu';
lgd.FontName = 'ubuntu';

axes.Title.FontSize = font_size;
axes.XAxis.FontSize = font_size;
axes.YAxis.FontSize = font_size;

% Normalized average cost per urgency type plot
subplot(1,2,2);
bar(x_axis(1:param.n_mu), reshape(c_bar_down_mu(1,:).', [], 1));
hold on;
bar(x_axis(param.n_mu+1:2*param.n_mu), reshape(c_bar_down_mu(2,:).', [], 1));
bar(x_axis(2*param.n_mu+1:3*param.n_mu), reshape(c_bar_down_mu(3,:).', [], 1));

axis tight;
axes = gca;

axes.Title.String = 'Average cost per urgency type';

axes.Title.FontName = 'ubuntu';
axes.XAxis.FontName = 'ubuntu';
axes.YAxis.FontName = 'ubuntu';
lgd.FontName = 'ubuntu';

axes.Title.FontSize = font_size;
axes.XAxis.FontSize = font_size;
axes.YAxis.FontSize = font_size;

sgtitle('Performance comparison per type', 'FontSize', 20, 'FontName', 'ubunutu', 'FontWeight', 'bold');