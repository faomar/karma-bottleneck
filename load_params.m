function param = load_params()

%% Setting parameters
% Number of agents (playing the karma game)
param.N = 9000;

% Discretization time unit [minutes]
param.dt = 15;

% Total capacity
param.S = 60 * param.dt;    % vehicles / timestep
param.s = param.S / param.N;    % mass of vehicles / timestep

% Fast lane capacity
param.S_1 = round(0.2 * param.S);   % vehicles / timestep
% param.S_1 = round(0.0 * param.S);   % vehicles / timestep
param.s_1 = param.S_1 / param.N;    % mass of vehicles / timestep

% Slow lane capacity
param.S_2 = param.S - param.S_1;    % vehicles / timestep
param.s_2 = param.S_2 / param.N;    % mass of vehicles / timestep

% Fast lane capacity buffer
param.epsilon = 0.0001;

% Queue delay sensitivity
param.alpha = 6.4 / 60 * param.dt;  % per timestep

% Early schedule sensitivity
% param.beta = 3.9 / 60 * param.dt;   % per timestep
param.beta = 25 / 4 * param.s * param.alpha;   % this makes t_start, t_end, t_peak integer away from t_star

% Late schedule sensitivity
% param.gamma = 15.21 / 60 * param.dt;    % per timestep
param.gamma = 25 * param.s * param.alpha;   % this makes t_start, t_end, t_peak integer away from t_star

% Number of departure/arrival timesteps
param.T = ceil(1 + param.N / param.S);  % adding 1 for discretization buffer

% Departure/arrival timesteps
param.t = (1 : param.T).';

% Departure/arrival times in minutes
param.t_min = (param.t - 1) * param.dt;    

% Desired arrival time types
% First type is the one corresponding to homogeneous population
param.tstar = round(1 + param.gamma / (param.beta + param.gamma) * param.N / param.S);
% (Optional) second type has t* before
% param.tstar = [param.tstar; round(1 + (param.tstar - 1) / 2)];
param.tstar = [param.tstar; 1];     % Few commuters want to depart very early

% Desired arrival time types in minutes
param.tstar_min = (param.tstar - 1) * param.dt;

% Number of desired arrival time types
param.n_tstar = length(param.tstar);

% Number of urgency types
param.n_mu = 1;

% Future awareness types/parameters
% If column vector, different future awareness types
% If row vector, different future awareness parameters
param.Alpha = 0.99;

% Number of future awareness types
param.n_alpha = size(param.Alpha, 1);

% Number of future awareness parameters
param.n_alpha_comp = size(param.Alpha, 2);

% Type distribution
% Desired arrival time type distribution
if param.n_tstar == 1
    param.g_up_tstar = 1.0;
else
%     param.g_up_tstar = [0.8; 0.2];  % Single peak
%     param.g_up_tstar = [0.5; 0.5];  % Double peak
    param.g_up_tstar = [1 - param.s_1; param.s_1];  % Few commuters want to depart very early
end
valid_dist = length(param.g_up_tstar) == param.n_tstar && min(param.g_up_tstar) >= 0 && sum(param.g_up_tstar) == 1;
assert(valid_dist, 'Invalid desired arrival time type distribution');

% Urgency type distribution
param.g_up_mu = 1 / param.n_mu * ones(param.n_mu, 1);
valid_dist = length(param.g_up_mu) == param.n_mu && min(param.g_up_mu) >= 0 && sum(param.g_up_mu) == 1;
assert(valid_dist, 'Invalid urgency type distribution');

% Future awareness type distribution
param.g_up_alpha = 1 / param.n_alpha * ones(param.n_alpha, 1);
valid_dist = length(param.g_up_alpha) == param.n_alpha && min(param.g_up_alpha) >= 0 && sum(param.g_up_alpha) == 1;
assert(valid_dist, 'Invalid future awareness type distribution');

% Joint type distribution
param.g_up_tstar_mu_alpha = einsum('ij,kj,lj->ikl', param.g_up_tstar, param.g_up_mu, param.g_up_alpha);

% Number of urgency values
% Set to maximum size of urgency sets of types
if param.n_mu == 2
    param.n_u = 1;
else
    param.n_u = 2;
end

% Set of urgency values, per urgency type
param.U_down_mu = zeros(param.n_mu, param.n_u);
if param.n_mu == 1
%     param.U_down_mu(1,:) = [1, 11];
    param.U_down_mu(1,:) = [1, 6];
%     param.U_down_mu(1,:) = [1, 3];
elseif param.n_mu == 2
    param.U_down_mu(1,:) = 1;
    param.U_down_mu(2,:) = 6;
else
    param.U_down_mu(1,:) = [1, 11];
    param.U_down_mu(2,:) = [1, 6];
    param.U_down_mu(3,:) = [1, 3];
    
    if param.n_mu == 4
        param.U_down_mu(4,:) = [2, 2];
    end
end

% Minimum and maximum urgency value per type
param.umin_down_mu = min(param.U_down_mu, [], 2);
param.umax_down_mu = max(param.U_down_mu, [], 2);

% Urgency Markov chain
param.phi_down_mu_u_up_un = zeros(param.n_mu, param.n_u, param.n_u);
if param.n_mu == 1
%     param.phi_down_mu_u_up_un(1,:,:) = [0.9, 0.1;
%                                         0.9, 0.1];
    param.phi_down_mu_u_up_un(1,:,:) = [0.8, 0.2;
                                        0.8, 0.2];
%     param.phi_down_mu_u_up_un(1,:,:) = [0.5, 0.5;
%                                         0.5, 0.5];
elseif param.n_mu == 2
    param.phi_down_mu_u_up_un(1,:,:) = 1;
    param.phi_down_mu_u_up_un(2,:,:) = 1;
else
    param.phi_down_mu_u_up_un(1,:,:) = [0.9, 0.1;
                                        0.9, 0.1];
    param.phi_down_mu_u_up_un(2,:,:) = [0.8, 0.2;
                                        0.8, 0.2];
    param.phi_down_mu_u_up_un(3,:,:) = [0.5, 0.5;
                                        0.5, 0.5];
    if param.n_mu == 4
        param.phi_down_mu_u_up_un(4,:,:) = [1.0, 0.0;
                                            1.0, 0.0];
    end
end
valid_phi = true;
for i_mu = 1 : param.n_mu
    for i_u = 1 : param.n_u
        valid_phi = valid_phi ...
            && min(param.phi_down_mu_u_up_un(i_mu,i_u,:)) >= 0 ...
            && sum(param.phi_down_mu_u_up_un(i_mu,i_u,:)) == 1;
        if ~valid_phi
            break;
        end
    end
    if ~valid_phi
        break;
    end
end
assert(valid_phi, 'Invalid urgency Markov chain');

% Stationary distribution of urgency Markov chain
% Also detect if urgency is i.i.d, in which case we can simply sample from
% stationary distribution. This is the case when all rows of the transition
% matrix are equal to the stationary distribution
param.prob_down_mu_up_u = zeros(param.n_mu, param.n_u);
param.u_iid = true(param.n_mu, 1);
for i_mu = 1 : param.n_mu
    param.prob_down_mu_up_u(i_mu,:) = stat_dist(squeeze(param.phi_down_mu_u_up_un(i_mu,:,:)));
    for i_u = 1 : param.n_u
        if norm(param.prob_down_mu_up_u(i_mu,:).' - squeeze(param.phi_down_mu_u_up_un(i_mu,i_u,:)), inf) >= 1e-10
            param.u_iid(i_mu) = false;
            break;
        end
    end
end

% Average urgency per urgency type
param.u_bar_down_mu = einsum('ij,ij->i', param.prob_down_mu_up_u, param.U_down_mu);

% Average amount of karma
param.k_bar = 10;

% Analytical t_peak where maximum queue arises
if param.n_tstar == 1 || param.g_up_tstar(2) <=  (param.tstar(2) - 1) / (param.T - 1)
    param.tpeak = param.tstar(1) - param.beta * param.gamma / (param.alpha * (param.beta + param.gamma) * param.s);
    param.multi_peak = false;
else
    param.tpeak = param.tstar - param.beta * param.gamma / (param.alpha * (param.beta + param.gamma) * param.s) * param.g_up_tstar;
    param.multi_peak = true;
end
param.tpeak_min = (param.tpeak - 1) * param.dt;

% Karma transition mechanism
% 0 => Winners pay bid to society, uniform redistribution
% 1 => All pay bid to society, uniform redistribution
% 2 => Winners pay bid to society, redistribution to losers + 0 bidders only
% 3 => Winners pay bid to society, linear time redistribution away from
% tminre, zero redistribution at tminre
% 4 => Winners pay bid to society, linear time redistribution away from
% tminre, r[1] = t1_tminre_ratio * r[tminre]
% 5 => Winners pay bid to society, more redistribution after t_peak
param.transition_mechanism = 0;

% Linear time redistribution time for which redistibution is 0
% (only applicable if transition_mechanism = 3
switch param.transition_mechanism
    case {3, 4}
        % param.tminre = param.tstar(1);
%         param.tminre = round(param.tpeak(1));
        param.tminre = round(param.T / 2);
        
        if param.transition_mechanism == 4
            param.t1_tminre_ratio = param.tminre;
        end
end

end