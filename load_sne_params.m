function sne_param = load_sne_params(param)

%% Set of karma values
% Maximum karma level. Used as a starting point
sne_param.k_max = 60;
% sne_param.k_max = 1;

% Tolerance for maximum probability of k_max
sne_param.max_sigma_k_max = 1e-3;

% Set of all karma values
sne_param.K = (0 : sne_param.k_max).';

% Number of karma values
sne_param.n_k = length(sne_param.K);

% Number of karma values duplicated T times
sne_param.n_k_down_t = sne_param.n_k * ones(param.T, 1);

% Where is k_bar?
sne_param.i_k_bar = find(sne_param.K == param.k_bar);

% Number of states
sne_param.n_x = param.n_u * sne_param.n_k;

% Set of output values
sne_param.O = [1; 2];

% Number of output values
sne_param.n_o = length(sne_param.O);


%% Initialization
% Departure time policy initialization
% 0 => Depart at ideal arrival time
% 1 => Uniform random
% 2 => According to analytical equilibrium
% 10 => From file
sne_param.t_policy_initialization = 2;

% Bidding policy initialization
% 0 => Bid urgency
% 1 => Bid 0.5 * u / u_max * k (~ 'bid half if urgent')
% 2 => Bid 1 * u / u_max * k (~ 'bid all if urgent')
% 3 => Bid uniform random
% 10 => From file
sne_param.b_policy_initialization = 1;

% Karma distribution initialization
% 0 => All agents have average karma k_bar
% 1 => Uniform distribution over [0 : 2 * k_bar]
% 10 => From file
sne_param.karma_initialization = 0;

% Initialization file (for initialization from file option)
sne_param.initialization_file = 'results/k_bar-10-alpha-0.99.mat';

%% Computation parameters
% Bounded rationality
sne_param.lambda = 1000;
% sne_param.lambda = 10;

% Tolerance for convergence of equilibrium policy
sne_param.sne_pi_tol = 1e-6;

% Tolerance for convergence of type-state distribution
sne_param.sne_d_tol = 1e-6;

% Maximum number of iterations
sne_param.max_iter = 10000;
% sne_param.max_iter = 100000;

% Tolerance for convergence of value function
sne_param.V_tol = 1e-6;

% Maximum number of iterations for convergence of value function
sne_param.V_max_iter = 1000;

% Tolerance for best response single-stage deviation
sne_param.br_Q_tol = 1e-6;

% Discrete step size
sne_param.dt = 0.1;

% Policy update rate
sne_param.eta = 0.1;
% sne_param.eta = 0.01;

% Keep history of evolution
sne_param.store_hist = false;

% Do plots
sne_param.plot = false;

% Plot font size
sne_param.plot_font_size = 10;

% Save results
sne_param.save = true;

end